
const elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

elixir((mix) => {

    mix.sass([
        'app.scss',
    ], 'public/bundled/css/app.css');

    mix.sass([
      'homepage/main.scss'
    ],'public/bundled/css/homepage.css');

    mix.sass([
        'auth/main.scss',
        'homepage/footer.scss'
    ], 'public/bundled/css/auth.css');

    mix.copy([
        'node_modules/font-awesome/fonts'
    ], 'public/bundled/fonts');

    mix.sass([
        'admin/main.scss',
    ], 'public/bundled/css/admin.css');

    //////////////////////
    // Homepage scripts //
    //////////////////////
    mix.browserify([
        'homepage/plugins.js',
        '../../../node_modules/angular-utils-pagination/dirPagination.js',
        'components/parseView.js',
        'homepage/app.js',
        'components/angular-template-bracket.js',

        /**
         * Services
         */
        'homepage/homepage.service.js',

        /**
         * Controllers
         */
        'homepage/homepage.controller.js',
    ], 'public/bundled/js/homepage.js');

    ////////////////////////
    // Shop admin scripts //
    ////////////////////////
    mix.browserify([
        'admin/plugins.js',
        '../../../node_modules/isteven-angular-multiselect/isteven-multi-select.js',
        '../../../node_modules/ng-img-crop/compile/unminified/ng-img-crop.js',
        '../../../node_modules/angular-utils-pagination/dirPagination.js',
        'components/parseView.js',
        'admin/app.js',
        'components/angular-template-bracket.js',
        'admin/routes.js',

        /**
         * Directives
         */
        'admin/angular/directives/delete-model.directive.js',
        'admin/angular/directives/delete-models.directive.js',
        'admin/angular/directives/file-model.directive.js',
        'admin/angular/directives/file-uploader.directive.js',
        'admin/angular/directives/file-cropper.directive.js',
        'admin/angular/directives/loading-snippet.directive.js',
        'admin/angular/directives/multi-select.directive.js',

        /**
         * Services
         */
        'admin/angular/services/auth-http.service.js',
        'admin/angular/services/file-model.service.js',
        'admin/pet/breed/breed.service.js',
        'admin/pet/status/status.service.js',
        'admin/pet/type/type.service.js',
        'admin/pet/pet.service.js',
        'admin/user/auth.service.js',

        /**
         * Controllers
         */
        'admin/pet/breed/breed.controller.js',
        'admin/pet/status/status.controller.js',
        'admin/pet/type/type.controller.js',
        'admin/pet/pet.controller.js',
        'admin/pet/pet-management.controller.js',
        'admin/user/profile.controller.js',

    ], 'public/bundled/js/admin.js');

    mix.browserSync({
       proxy: 'localhost:8000',
       files: [
         'public/bundled/**/*.css',
         'public/bundled/**/*.js',
         'resources/views/**/*.blade.php',
       ]
   });
});
