-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 26, 2017 at 08:49 PM
-- Server version: 5.7.19-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pets_world`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `views` int(11) UNSIGNED NOT NULL,
  `number_of_like` int(11) UNSIGNED NOT NULL,
  `category_id` int(11) UNSIGNED NOT NULL,
  `is_public` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `article_category`
--

CREATE TABLE `article_category` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `article_comments`
--

CREATE TABLE `article_comments` (
  `id` int(11) UNSIGNED NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `number_of_likes` int(11) UNSIGNED NOT NULL,
  `article_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `article_tag`
--

CREATE TABLE `article_tag` (
  `id` int(11) UNSIGNED NOT NULL,
  `article_id` int(11) UNSIGNED NOT NULL,
  `tag_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `authorities`
--

CREATE TABLE `authorities` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `authorities`
--

INSERT INTO `authorities` (`id`, `name`) VALUES
(1, 'System administrator'),
(2, 'Shop owner'),
(3, 'Normal user');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `status_id` int(11) UNSIGNED NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `blog_status`
--

CREATE TABLE `blog_status` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `blog_tags`
--

CREATE TABLE `blog_tags` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `feedbacks`
--

CREATE TABLE `feedbacks` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pets`
--

CREATE TABLE `pets` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `sex` int(1) NOT NULL DEFAULT '0',
  `breed_id` int(11) UNSIGNED NOT NULL,
  `type_id` int(11) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL DEFAULT '0',
  `height` float UNSIGNED NOT NULL DEFAULT '0',
  `price` float UNSIGNED NOT NULL DEFAULT '0',
  `shop_id` int(11) UNSIGNED NOT NULL,
  `photo_landscape` varchar(200) DEFAULT NULL,
  `photo_square` varchar(200) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pets`
--

INSERT INTO `pets` (`id`, `name`, `description`, `sex`, `breed_id`, `type_id`, `weight`, `height`, `price`, `shop_id`, `photo_landscape`, `photo_square`, `created_at`, `updated_at`) VALUES
(1, 'Lyli', 'null', 1, 13, 2, 3.5, 10, 2000000, 1, '1503752247_landscape.jpg', '1503752247_square.jpg', 1503752247, 1503752505),
(2, 'Sugar', 'undefined', 0, 11, 2, 2, 7, 1500000, 1, '1503752470_landscape.jpg', '1503752470_square.jpg', 1503752470, 1503752470),
(3, 'Pen', 'undefined', 1, 6, 1, 10, 25, 3200000, 1, '1503752579_landscape.jpg', '1503752579_square.jpg', 1503752579, 1503752579),
(4, 'Anie', 'undefined', 0, 7, 1, 15.8, 23, 2300000, 1, '1503752686_landscape.jpg', '1503752686_square.jpg', 1503752686, 1503752686),
(5, 'Lulu', 'undefined', 0, 5, 1, 7.5, 14, 2400000, 1, '1503752838_landscape.jpg', '1503752838_square.jpg', 1503752838, 1503752838),
(6, 'Coco', 'undefined', 0, 22, 4, 1, 10, 1000000, 1, '1503753907_landscape.jpg', '1503753907_square.jpg', 1503753907, 1503753907),
(7, 'Buddy', 'undefined', 1, 20, 4, 1, 12, 1000000, 1, '1503754003_landscape.jpg', '1503754004_square.jpg', 1503754003, 1503754003),
(8, 'Sunny', 'undefined', 0, 17, 3, 1, 7, 1000000, 1, '1503754139_landscape.jpg', '1503754139_square.jpg', 1503754139, 1503754139),
(9, 'McLovin', 'undefined', 1, 15, 3, 1, 10, 1000000, 1, '1503754209_landscape.jpg', '1503754209_square.jpg', 1503754209, 1503754209),
(10, 'Pumpkin', 'undefined', 0, 15, 3, 1, 8, 1000000, 1, '1503754260_landscape.jpg', '1503754260_square.jpg', 1503754260, 1503754260),
(11, 'Nemo', 'undefined', 1, 16, 3, 1, 9, 1000000, 1, '1503754320_landscape.jpg', '1503754320_square.jpg', 1503754320, 1503754320),
(12, 'Boo', 'undefined', 1, 14, 3, 1, 9, 1000000, 1, '1503754420_landscape.jpg', '1503754420_square.jpg', 1503754420, 1503754420),
(13, 'Goldie', 'undefined', 1, 16, 3, 1, 8, 1000000, 1, '1503754463_landscape.jpg', '1503754463_square.jpg', 1503754463, 1503754463);

-- --------------------------------------------------------

--
-- Table structure for table `pets_status`
--

CREATE TABLE `pets_status` (
  `pets_id` int(11) UNSIGNED NOT NULL,
  `status_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pets_status`
--

INSERT INTO `pets_status` (`pets_id`, `status_id`) VALUES
(2, 1),
(1, 1),
(1, 3),
(3, 1),
(4, 1),
(5, 2),
(5, 3),
(6, 1),
(7, 1),
(7, 3),
(8, 1),
(9, 1),
(10, 1),
(10, 3),
(11, 1),
(12, 2),
(12, 3),
(13, 1),
(13, 3);

-- --------------------------------------------------------

--
-- Table structure for table `pet_breeds`
--

CREATE TABLE `pet_breeds` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_id` int(11) UNSIGNED NOT NULL,
  `photo` varchar(200) DEFAULT NULL,
  `shop_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pet_breeds`
--

INSERT INTO `pet_breeds` (`id`, `name`, `description`, `type_id`, `photo`, `shop_id`) VALUES
(4, 'Akita', NULL, 1, '1503751677.jpg', 1),
(5, 'Alaskan Malamute', 'undefined', 1, '1503751727.jpg', 1),
(6, 'American Eskimo', 'undefined', 1, '1503751794.jpg', 1),
(7, 'Australian Shepherd', NULL, 1, '1503751855.jpg', 1),
(8, 'Basenjis', 'undefined', 1, '1503751874.jpg', 1),
(9, 'Belgian', 'undefined', 1, '1503751936.jpg', 1),
(10, 'Abyssinian', 'undefined', 2, '1503752010.jpg', 1),
(11, 'Bengal', NULL, 2, '1503752042.jpg', 1),
(12, 'Colorpoint Shorthair', 'undefined', 2, '1503752067.jpg', 1),
(13, 'Nebelung', 'undefined', 2, '1503752091.jpg', 1),
(14, 'Goldfish', NULL, 3, '1503753109.jpg', 1),
(15, 'Achilles Tang', 'undefined', 3, '1503753154.jpg', 1),
(16, 'Red Platy', 'undefined', 3, '1503753184.jpg', 1),
(17, 'Tiger Barb', 'undefined', 3, '1503753222.jpg', 1),
(18, 'Parakeet', 'undefined', 4, '1503753298.jpg', 1),
(19, 'Cockatiel', 'undefined', 4, '1503753347.jpg', 1),
(20, 'Parrot', 'undefined', 4, '1503753391.jpg', 1),
(21, 'Senegal Parrot', 'undefined', 4, '1503753414.jpg', 1),
(22, 'Accentor', 'undefined', 4, '1503753442.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pet_details`
--

CREATE TABLE `pet_details` (
  `id` int(11) UNSIGNED NOT NULL,
  `pet_id` int(11) UNSIGNED NOT NULL,
  `specification_id` int(11) UNSIGNED NOT NULL,
  `value` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pet_specifications`
--

CREATE TABLE `pet_specifications` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit_type_id` int(11) UNSIGNED NOT NULL,
  `unit_id` int(11) UNSIGNED NOT NULL,
  `pet_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pet_specifications`
--

INSERT INTO `pet_specifications` (`id`, `name`, `unit_type_id`, `unit_id`, `pet_type_id`) VALUES
(1, 'Height', 2, 6, 1),
(2, 'Weight', 1, 2, 1),
(3, 'Height', 2, 6, 2),
(4, 'Weight', 1, 2, 2),
(5, 'Length', 2, 6, 3),
(6, 'Height', 2, 6, 4);

-- --------------------------------------------------------

--
-- Table structure for table `pet_status`
--

CREATE TABLE `pet_status` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `shop_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pet_status`
--

INSERT INTO `pet_status` (`id`, `name`, `description`, `shop_id`) VALUES
(1, 'Normal', NULL, 1),
(2, 'Sick', '', 1),
(3, 'Adopted', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pet_types`
--

CREATE TABLE `pet_types` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `shop_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pet_types`
--

INSERT INTO `pet_types` (`id`, `name`, `description`, `shop_id`) VALUES
(1, 'Dog', NULL, 1),
(2, 'Cat', NULL, 1),
(3, 'Fish', NULL, 1),
(4, 'Bird', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pet_units`
--

CREATE TABLE `pet_units` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_id` int(11) UNSIGNED NOT NULL,
  `short_name` varchar(10) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pet_units`
--

INSERT INTO `pet_units` (`id`, `name`, `type_id`, `short_name`) VALUES
(1, 'gram', 1, '(g)'),
(2, 'kilogram', 1, '(kg)'),
(3, 'pounds', 1, '(lbs)'),
(4, 'ounce', 1, '(oz)'),
(5, 'milimetres', 2, '(mm)'),
(6, 'centimetres', 2, '(cm)'),
(7, 'metres', 2, '(m)'),
(8, 'inch', 2, '(in)'),
(9, 'foot', 2, '(ft)'),
(10, 'yard', 2, '(yd)'),
(11, 'age', 3, NULL),
(12, 'month', 3, NULL),
(13, 'year', 3, NULL),
(14, 'day', 3, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pet_unit_types`
--

CREATE TABLE `pet_unit_types` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pet_unit_types`
--

INSERT INTO `pet_unit_types` (`id`, `name`) VALUES
(1, 'weight'),
(2, 'length'),
(3, 'time');

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE `shops` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `country` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` int(20) NOT NULL,
  `owner_id` int(11) UNSIGNED NOT NULL,
  `status_id` int(11) UNSIGNED NOT NULL,
  `photo` varchar(200) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shop_accessories`
--

CREATE TABLE `shop_accessories` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `category_id` int(11) UNSIGNED NOT NULL,
  `unit` varchar(10) NOT NULL,
  `quantity` int(11) UNSIGNED NOT NULL,
  `price` float UNSIGNED NOT NULL,
  `shop_id` int(11) UNSIGNED NOT NULL,
  `origin_country` varchar(50) DEFAULT NULL,
  `created_at` int(11) UNSIGNED DEFAULT NULL,
  `updated_at` int(11) UNSIGNED DEFAULT NULL,
  `produced_at` int(11) UNSIGNED DEFAULT NULL,
  `expired_at` int(11) UNSIGNED DEFAULT NULL,
  `photo` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shop_accessory_categories`
--

CREATE TABLE `shop_accessory_categories` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `shop_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shop_services`
--

CREATE TABLE `shop_services` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `price` float UNSIGNED NOT NULL,
  `shop_id` int(11) UNSIGNED NOT NULL,
  `photo` varchar(200) DEFAULT NULL,
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shop_status`
--

CREATE TABLE `shop_status` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `shop_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trainers`
--

CREATE TABLE `trainers` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `shop_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `firstname` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `birth` int(11) DEFAULT NULL,
  `country` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `confirmation_token` varchar(100) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `api_token` varchar(60) DEFAULT NULL,
  `current_shop_id` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `authority_id` int(11) UNSIGNED NOT NULL,
  `photo` varchar(200) DEFAULT NULL,
  `is_confirmed` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `birth`, `country`, `city`, `address`, `phone`, `email`, `password`, `confirmation_token`, `remember_token`, `api_token`, `current_shop_id`, `created_at`, `updated_at`, `authority_id`, `photo`, `is_confirmed`) VALUES
(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pvtri96@gmail.com', '$2y$10$0pMq1YdVIRE7Vaed0Dy0s.R7S0JIZSeCqYpWDIXlxGftwcMaW2lxS', '', 'apkxKASUUqtcaoSe0959ycDsHnQL6huLcP8aMKehuGGuXveegr80wCyI7i5j', 'g4Lf0uCxiEgb9OET2XxyFiQ8jqgrjR2URlg1955tI6cKj03oujRXyb0KoDB8', 1, 1490843153, 1490843153, 1, NULL, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `article_category`
--
ALTER TABLE `article_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `article_comments`
--
ALTER TABLE `article_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `article_tag`
--
ALTER TABLE `article_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `authorities`
--
ALTER TABLE `authorities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_status`
--
ALTER TABLE `blog_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_tags`
--
ALTER TABLE `blog_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedbacks`
--
ALTER TABLE `feedbacks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pets`
--
ALTER TABLE `pets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pet_breeds`
--
ALTER TABLE `pet_breeds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pet_details`
--
ALTER TABLE `pet_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pet_specifications`
--
ALTER TABLE `pet_specifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pet_status`
--
ALTER TABLE `pet_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pet_types`
--
ALTER TABLE `pet_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pet_units`
--
ALTER TABLE `pet_units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pet_unit_types`
--
ALTER TABLE `pet_unit_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_accessories`
--
ALTER TABLE `shop_accessories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_accessory_categories`
--
ALTER TABLE `shop_accessory_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_services`
--
ALTER TABLE `shop_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_status`
--
ALTER TABLE `shop_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trainers`
--
ALTER TABLE `trainers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `article_category`
--
ALTER TABLE `article_category`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `article_comments`
--
ALTER TABLE `article_comments`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `authorities`
--
ALTER TABLE `authorities`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `blog_status`
--
ALTER TABLE `blog_status`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `blog_tags`
--
ALTER TABLE `blog_tags`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `feedbacks`
--
ALTER TABLE `feedbacks`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pets`
--
ALTER TABLE `pets`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `pet_breeds`
--
ALTER TABLE `pet_breeds`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `pet_details`
--
ALTER TABLE `pet_details`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pet_specifications`
--
ALTER TABLE `pet_specifications`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `pet_status`
--
ALTER TABLE `pet_status`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pet_types`
--
ALTER TABLE `pet_types`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `pet_units`
--
ALTER TABLE `pet_units`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `pet_unit_types`
--
ALTER TABLE `pet_unit_types`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `shops`
--
ALTER TABLE `shops`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_accessories`
--
ALTER TABLE `shop_accessories`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_accessory_categories`
--
ALTER TABLE `shop_accessory_categories`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_services`
--
ALTER TABLE `shop_services`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop_status`
--
ALTER TABLE `shop_status`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trainers`
--
ALTER TABLE `trainers`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
