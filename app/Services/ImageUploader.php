<?php

namespace App\Services;

use Image;

class ImageUploader
{
    const ADJUSTED_HEIGHT_MEDIUM = 300;
    const ADJUSTED_HEIGHT_SMALL = 150;
    const DIRECTORY = 'upload/images/';
    private $image;
    private $time;

    public function __construct($image, $time = null)
    {
        if ($time == null) {
            $time = time();
        }
        $this->time = $time;
        $this->image = $image;

        $this->upload();
    }

    public function upload(){
        $sizes = self::getSizes();
        foreach ($sizes as $size) {
            if($size['name'] == 'large') {
                $imageName = $this->imageName();
                $this->image->move(public_path($size['path']), $imageName);
            }else{
                $this->resizeImage($size['path'], $size['name']);
            }
        }
    }

    public function imageName(){
        return $this->time.'.'.$this->image->getClientOriginalExtension();
    }

    private function resizeImage($path, $name){
        $img = Image::make($this->image->getRealPath());
        $imageName = self::imageName();

        $width = $img->width();
        $height = $img->height();
        $ratio = $width / $height;

        switch ($name) {
            case 'medium':
                $adjustedWidth = self::ADJUSTED_HEIGHT_MEDIUM * $ratio;
                $adjustedHeight = self::ADJUSTED_HEIGHT_MEDIUM;
                break;

            case 'small':
                $adjustedWidth = self::ADJUSTED_HEIGHT_SMALL * $ratio;
                $adjustedHeight = self::ADJUSTED_HEIGHT_SMALL;
                break;
        }

        $img->resize($adjustedWidth, $adjustedHeight, function($constraint){
            $constraint->aspectRatio();
        })->save(public_path($path.'/'.$imageName));
    }

    private static function getSizes(){
        $sizes = [
            [
                'name' => 'small',
                'path' => self::DIRECTORY . 'small'
            ],
            [
                'name' => 'medium',
                'path' => self::DIRECTORY . 'medium'
            ],
            [
                'name' => 'large',
                'path' => self::DIRECTORY . 'large'
            ]
        ];

        foreach ($sizes as $size) {
            $pathName = self::DIRECTORY . $size['name'];
            if (!is_dir('upload')) {
                mkdir('upload');
            }
            if (!is_dir(self::DIRECTORY)) {
                mkdir(self::DIRECTORY);
            }
            if (!is_dir($pathName)) {
                mkdir($pathName);
            }
        }
        return $sizes;
    }
}
