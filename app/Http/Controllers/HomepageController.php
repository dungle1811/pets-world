<?php

namespace App\Http\Controllers;
use App\Model\Pet;

use Illuminate\Http\Request;

class HomepageController extends Controller
{	
	public function getIndex()
	{
        return view('homepage.index');
	}
	
	public function getPets()
	{
		return view('homepage.pets');
	}

   	public function getPetDetail($id)
   	{
   		$pet = Pet::find($id);
   		return view('homepage.pet-detail')->with([
   			'pet' => $pet
   		]);
   	}
}
