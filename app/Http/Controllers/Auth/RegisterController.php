<?php

namespace App\Http\Controllers\Auth;

use App\Model\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests\Auth\RegisterRequest;
use Session;
use App\Mail\ConfirmationMail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function getRegister()
    {
        return view('auth.register');
    }

    public function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|email|max:50|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    public function create(array $data)
    {
        return $user = User::create([
            'email'        => $data['email'],
            'password'     => bcrypt($data['password']),
            'authority_id' => '1',
            'created_at'   => time(),
            'updated_at'   => time(),
            'api_token'    => str_random(60),
        ]);
    }

    public function postRegister(RegisterRequest $request)
    {
        $input = $request->all();
        $validator = $this->validator($input);
        if($validator->passes()){
            $data = $this->create($input)->toArray();
            $data['confirmation_token'] = str_random(60);
            $user = User::find($data['id']);
            $user->confirmation_token = $data['confirmation_token'];
            $user->save();
            Mail::send('emails.confirmation', $data, function($message) use($data){
                $message->to($data['email']);
                $message->subject(trans('auth.confirmation_title'));
            });
            Session::push('messages', [trans('auth.confirmation_content'), 'info']);
            return redirect()->route('view.auth.login');
        }
        Session::push('messages',$validator->errors);
        return redirect()->route('view.auth.login');
    }

    public function confirm($confirmation_token)
    {
        $user = User::where('confirmation_token', $confirmation_token)->first();
        if(!is_null($user)){
            $user->is_confirmed = 1;
            $user->confirmation_token = '';
            $user->save();
            Session::push('messages', [trans('auth.confirm_successful'), 'info']);
            return redirect()->route('view.auth.login');
        }
        Session::push('messages', [trans('auth.confirm_fail')]);
        return redirect()->route('view.auth.login');
    }
}
