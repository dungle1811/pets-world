<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Session;
use App\Http\Auth\RegisterController;
use Validator;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function getLogin()
    {
        return view('auth.login');
    }

    public function postLogin(LoginRequest $request)
    {
        $user = [
            'email'    => $request->get('email'),
            'password' => $request->get('password')
        ];

        if(!Auth::attempt($user))
        {
            Session::push('messages', [trans('auth.failed')]);
            //redirect to
            return redirect()->back()->withInput();
        }

        if (Auth::user()->is_confirmed == 0) {
            Session::push('messages', [trans('auth.email_not_confirmed')]);
            Auth::logout();
            return redirect()->back();
        }

        $this->storeShopSession();

        //redirect to
        return redirect()->route('view.admin');
    }

    public function logout()
    {

        Auth::logout();

        return redirect()->route('view.auth.login');
    }

    public function storeShopSession()
    {
        $user = Auth::user();
        $user->current_shop_id = "1";
        $user->save();
    }
}
