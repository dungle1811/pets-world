<?php

namespace App\Http\Controllers\File;

use Validator;
use App\Services\ImageUploader;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FileController extends Controller
{
    public function test(Request $request){
        $validator = Validator::make($request->all(), [
            'image' => 'required|image|mimes:jpg,jpeg,png,gif,svg|max:2048',
        ]);
        $time = time();
        if ($validator->passes()) {
            $image = $request->image;
            $uploader = new ImageUploader($image, $time);
            return $uploader->imageName();
        }
        return response()->json([
            'error' => $validator->errors()->all()
        ]);
    }
}
