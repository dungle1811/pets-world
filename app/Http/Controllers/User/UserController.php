<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Model\User;
use Auth;

class UserController extends Controller
{
    public function get()
    {
      $user = Auth::guard('api')->user();
      $user->current_shop;
        return response()->json([
            'message' => trans('api.user_get_success'),
            'success' => true,
            'data'    => Auth::guard('api')->user()
        ]);
    }

    public function update(Request $request)
    {
        $user = Auth::guard('api')->user();

        $user->update($request->except(['api_token', 'remember_token', 'email', 'id', 'password']));

        return response()->json([
            'message' => trans('api.user_update_success'),
            'success' => true,
            'data'    => $user
        ]);
    }
}
