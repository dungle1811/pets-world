<?php

namespace App\Http\Controllers\Pet;

use App\Model\PetStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;

class PetStatusController extends Controller
{
  public function all(){
    return response()->json([
      'message' => trans('api.pet_status_all_success'),
      'success' => true,
      'data'    => PetStatus::all(),
    ]);
  }

  public function create(Request $request)
  {
    $validator = $this->validator($request);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.pet_status_create_fail'),
        'success' => false,
        'data'    => $validator->errors(),
      ], 422);
    }

    $pet_status= $request->all();
    $pet_status['shop_id'] = Auth::guard('api')->user()->current_shop_id;
    $pet_status= PetStatus::create($pet_status);

    return response()->json([
      'message' => trans('api.pet_status_create_success'),
      'success' => true,
      'data'    => $pet_status,
    ]);
  }

  public function update(Request $request)
  {
    $validator = $this->validator($request, true);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.pet_status_update_fail'),
        'success' => false,
        'data'    => $validator->errors(),
      ], 422);
    }

    $updated_pet_status = $request->all();

    $balance_of_status = PetStatus::where('id', '<>', $updated_pet_status['id'])->get();
    for ($i=0; $i < count($balance_of_status) ; $i++) {
      if ($updated_pet_status['name'] == $balance_of_status[$i]['name']) {
        return response()->json([
          'message' => trans('api.pet_status_update_fail'),
          'success' => false,
          'data' => 'This name is has already been taken.'
        ], 422);
      }
    }

    $updated_pet_status['updated_at']=time();
    $pet_status = PetStatus::find($request->id);
    $pet_status->update($updated_pet_status);

    return response()->json([
      'message' => trans('api.pet_status_update_success'),
      'success' => true,
      'data'    => $pet_status,
    ]);
  }
  public function delete(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'id' => 'required|exists:pet_status,id',
    ]);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.pet_status_delete_fail'),
        'success' => false,
        'data'    => $validator->error(),
      ], 422);
    }
    $pet_status = PetStatus::find($request->id);
    $pet_status->delete();

    return response()->json([
      'message' => trans('api.pet_status_delete_success'),
      'success' => true,
      'data'    => $pet_status,
    ]);
  }

  public function deleteMultiple(Request $request)
  {
    $statusChild_IDs = $request->statusChild_IDs;
    $status_to_delete = PetStatus::whereIn('id', $statusChild_IDs)->delete();
    return response()->json([
      'message' => 'Delete multiple status successful',
      'success' => true,
      'data'    => $statusChild_IDs,
    ]);
  }

  private function validator($request, $is_update = false)
  {
    $validator = Validator::make($request->all(), [
      'id' => $is_update ? 'required|exists:pet_status,id' : '',
      'name' => $is_update ? 'required' : 'required|unique:pet_status',
      'description' => '',
    ]);
    return $validator;
  }
}
