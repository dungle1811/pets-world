<?php

namespace App\Http\Controllers\Pet;

use App\Model\PetUnitType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class PetUnitTypeController extends Controller
{
  public function all(){
    return response()->json([
      'message' => trans('api.pet_unit_type_all_success'),
      'success' => true,
      'data'    => PetUnitType::with('units')->get(),
    ]);
  }
}
