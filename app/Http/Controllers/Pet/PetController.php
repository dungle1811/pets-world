<?php

namespace App\Http\Controllers\Pet;

use App\Model\Pet;
use App\Services\ImageUploader;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use DB;

class PetController extends Controller
{
  public function all(){

      return response()->json([
          'message' => trans('api.pet_all_success'),
          'success' => true,
          'data' => Pet::with('pet_breed', 'pet_status', 'pet_type', 'details')->get(),
      ]);
    }

    public function create(Request $request){
        $data = $request->all();
        $validator = $this->validator($request);
        if($validator->fails()){
          return response()->json([
              'message' => trans('api.pet_create_fail'),
              'success' => false,
              'data' => $validator->errors(),
          ], 422);
        }

        $data['created_at']=time();
        $data['updated_at']=time();
        $data['shop_id']= Auth::guard('api')->user()->current_shop_id;

        $pet= Pet::create($data);

        $statusData = array();
        // Split each status in pet_status string into an array
        $data = explode(',', $data['pet_status']);
        foreach($data as $statusChildID) {
          array_push($statusData, [
            'pets_id' => $pet->id,
            'status_id' => (int)$statusChildID
          ]);
        }
        $pet_status = DB::table('pets_status')->insert($statusData);

        return response()->json([
            'message' => trans('api.pet_create_success'),
            'success' => true,
            'data' => $pet,
            'pet_status' => $pet_status
        ]);
    }

    public function update(Request $request){
        $validator = $this->validator($request, true);
        if($validator->fails()){
          return response()->json([
              'message' => trans('api.pet_update_fail'),
              'success' => false,
              'data' => $validator->errors(),
          ], 422);
        }

        $updated_pet= $request->all();

        $balance_of_pets = Pet::where('id', '<>', $updated_pet['id'])->get();
        for ($i=0; $i < count($balance_of_pets) ; $i++) {
          if ($updated_pet['name'] == $balance_of_pets[$i]['name']) {
            return response()->json([
              'message' => trans('api.pet_update_fail'),
              'success' => false,
              'data' => 'This name is has already been taken.'
            ], 422);
          }
        }

        $balance_of_pet = Pet::where('id', '<>', $updated_pet['id'])->get();
        for ($i=0; $i < count($balance_of_pet) ; $i++) {
          if ($updated_pet['name'] == $balance_of_pet[$i]['name']) {
            return response()->json([
              'message' => trans('api.pet_update_fail'),
              'success' => false,
              'data' => 'This name is has already been taken.'
            ], 422);
          }
        }

        $updated_pet['updated_at']=time();
        $pet = Pet::find($request->id);
        $pet->update($updated_pet);

        $statusData = array();
        // Split each status in pet_status string into an array
        $data = explode(',', $updated_pet['pet_status']);
        foreach($data as $statusChildID) {
          array_push($statusData, [
            'pets_id' => $pet->id,
            'status_id' => (int)$statusChildID
          ]);
        }
        DB::table('pets_status')->where('pets_id', $updated_pet['id'])->delete();
        $pet_status = DB::table('pets_status')->insert($statusData);

        return response()->json([
            'message' => trans('api.pet_update_success'),
            'success' => true,
            'data' => $pet,
        ]);
    }

    public function updateLandscapePhoto(Request $request) {
        $pet = Pet::find($request->id);

        if (is_file($request->photo)) {
            $imageValidator = $this->imageValidator($request);
            if($imageValidator->fails()){
              return response()->json([
                'message' => 'fail',
                'success' => false,
                'data'    => $imageValidator->errors(),
              ], 422);
            }

            $photo = $request->photo;
            $landscape_photo_name = time() . '_landscape';
            $photo_uploader = new ImageUploader($photo, $landscape_photo_name);
            $pet['photo_landscape'] = $photo_uploader->imageName();

            $pet->save();

            return response()->json([
                'message' => trans('api.pet_create_success'),
                'success' => true,
                'data' => $pet,
            ]);
       }

       return response()->json([
           'message' => 'Not file!',
           'success' => false,
           'data' => $pet,
       ]);
    }

    public function updateSquarePhoto(Request $request) {
      $pet = Pet::find($request->id);

      if (is_file($request->photo)) {
          $imageValidator = $this->imageValidator($request);
          if($imageValidator->fails()){
            return response()->json([
              'message' => 'fail',
              'success' => false,
              'data'    => $imageValidator->errors(),
            ], 422);
          }

          $photo = $request->photo;
          $square_photo_name = time() . '_square';
          $photo_uploader = new ImageUploader($photo, $square_photo_name);
          $pet['photo_square'] = $photo_uploader->imageName();

          $pet->save();

          return response()->json([
              'message' => trans('api.pet_create_success'),
              'success' => true,
              'data' => $pet,
          ]);
     }

     return response()->json([
         'message' => 'Not file!',
         'success' => false,
         'data' => $pet,
     ]);
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'id' => 'required|exists:pets,id'
        ]);
        if($validator->fails()){

          return response()->json([
              'message' => trans('api.pet_delete_fail'),
              'success' => false,
              'data' => $validator->errors(),
          ], 422);
        }

        $pet = Pet::find($request->id);
        $pet->delete();

        return response()->json([
            'message' => trans('api.pet_delete_success'),
            'success' => true,
            'data' => $pet,
        ]);
    }

    private function validator($request, $is_update = false)
    {
      $validator= Validator::make($request->all(), [
         'id'  => $is_update ? 'required|exists:pets,id' : '',
         'name' => $is_update ? 'required' : 'required|unique:pets',
         'sex'  => 'required|boolean',
         'price'  => 'required|numeric',
         'breed_id' => 'required',
         'type_id'  => 'required',
      ]);
      return $validator;
    }

    private function imageValidator($request)
    {
      $validator = Validator::make($request->all(), [
        'photo' => 'image|mimes:jpg,jpeg,png,gif,svg|max:2048',
      ]);
      return $validator;
    }
}
