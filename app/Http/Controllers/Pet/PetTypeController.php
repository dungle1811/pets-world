<?php

namespace App\Http\Controllers\Pet;

use App\Model\PetType;
use App\Model\PetBreed;
use App\Model\PetSpecification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;

class PetTypeController extends Controller
{
    public function all(){
      return response()->json([
        'message' => trans('api.pet_type_all_success'),
        'success' => true,
        'data'    => PetType::with('specifications')->get(),
      ]);
    }

    public function create(Request $request)
    {
      $validator = $this->validator($request);
      if($validator->fails()){
        return response()->json([
          'message' => trans('api.pet_type_create_fail'),
          'success' => false,
          'data'    => $validator->errors(),
        ], 422);
      }

      $pet_type = $request->all();
      $pet_type['shop_id']= Auth::guard('api')->user()->current_shop_id;
      $pet_type = PetType::create($pet_type);

      return response()->json([
        'message' => trans('api.pet_type_create_success'),
        'success' => true,
        'data'    => $pet_type,
      ]);
    }

    public function update(Request $request)
    {
      $validator = $this->validator($request, true);
      if($validator->fails()){
        return response()->json([
          'message' => trans('api.pet_type_update_fail'),
          'success' => false,
          'data'    => $validator->errors(),
        ], 422);
      }

      $updated_pet_type = $request->all();

      $balance_of_type = PetType::where('id', '<>', $updated_pet_type['id'])->get();
      for ($i=0; $i < count($balance_of_type) ; $i++) {
        if ($updated_pet_type['name'] == $balance_of_type[$i]['name']) {
          return response()->json([
            'message' => trans('api.pet_type_update_fail'),
            'success' => false,
            'data' => 'This name is has already been taken.'
          ], 422);
        }
      }
      $pet_type = PetType::find($request->id);
      $pet_type->update($updated_pet_type);

      return response()->json([
        'message' => trans('api.pet_type_update_success'),
        'success' => true,
        'data'    => $pet_type,
      ]);
    }
    public function delete(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'id' => 'required|exists:pet_types,id',
      ]);
      if($validator->fails()){
        return response()->json([
          'message' => trans('api.pet_type_delete_validate_fail'),
          'success' => false,
          'data'    => $validator->error(),
        ], 422);
      }

      $pet_breeds = PetBreed::where('type_id', $request->id)->get();
      if (count($pet_breeds) > 0) {
        return response()->json([
          'message' => trans('api.pet_type_delete_fail'),
          'success' => false,
        ], 422);
      }

      $pet_type = PetType::find($request->id);
      PetSpecification::where('pet_type_id', $request->id)->delete();
      $pet_type->delete();

      return response()->json([
        'message' => trans('api.pet_type_delete_success'),
        'success' => true,
        'data'    => $pet_type,
      ]);
    }

    public function deleteMultiple(Request $request)
    {
      $type_IDs = $request->type_IDs;

      $pet_breeds = PetBreed::whereIn('type_id', $type_IDs)->get();
      if (count($pet_breeds) > 0) {
        return response()->json([
          'message' => trans('api.pet_types_delete_fail'),
          'success' => false,
        ], 422);
      }

      $types_to_delete = PetType::whereIn('id', $type_IDs)->delete();
      $types_to_delete = PetSpecification::whereIn('pet_type_id', $type_IDs)->delete();
      return response()->json([
        'message' => 'Delete multiple types successful',
        'success' => true,
        'data'    => $type_IDs,
      ]);
    }

    private function validator($request, $is_update = false)
    {
      $validator = Validator::make($request->all(), [
        'id' => $is_update ? 'required|exists:pet_types,id' : '',
        'name' => 'required|unique:pet_types',
        'description' => '',
      ]);
      return $validator;
    }
}
