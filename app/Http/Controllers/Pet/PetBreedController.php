<?php

namespace App\Http\Controllers\Pet;

use App\Model\PetBreed;
use App\Model\Pet;
use App\Services\ImageUploader;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;

class PetBreedController extends Controller
{
  public function all(){
    return response()->json([
      'message' => trans('api.pet_breed_all_success'),
      'success' => true,
      'data'    => PetBreed::with('pet_type')->get(),
    ]);
  }

  public function create(Request $request)
  {
    $pet_breed = $request->all();

    if (is_file($request->breed_photo)) {
      $imageValidator = $this->imageValidator($request);
      if($imageValidator->fails()){
        return response()->json([
          'message' => 'fail',
          'success' => false,
          'data'    => $imageValidator->errors(),
        ], 422);
      }

      $photo = $request->breed_photo;

      $time = time();
      $uploader = new ImageUploader($photo, $time);

      $pet_breed['photo'] = $uploader->imageName();
    }

    $validator = $this->validator($request);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.pet_breed_create_fail'),
        'success' => false,
        'data'    => $validator->errors(),
      ], 422);
    }

    $pet_breed['shop_id']= Auth::guard('api')->user()->current_shop_id;
    $pet_breed['updated_at'] = time();

    $pet_breed = PetBreed::create($pet_breed);

    return response()->json([
      'message' => trans('api.pet_breed_create_success'),
      'success' => true,
      'data'    => $pet_breed,
    ]);
  }

  public function update(Request $request)
  {
    $updated_pet_breed = $request->all();

    $balance_of_breed = PetBreed::where('id', '<>', $updated_pet_breed['id'])->get();
    for ($i=0; $i < count($balance_of_breed) ; $i++) {
      if ($updated_pet_breed['name'] == $balance_of_breed[$i]['name']) {
        return response()->json([
          'message' => trans('api.pet_breed_update_fail'),
          'success' => false,
          'data' => 'This name is has already been taken.'
        ], 422);
      }
    }

    $old_breed = PetBreed::where('id', $request->id)->first();

    if (is_file($request->breed_photo)) {
      $imageValidator = $this->imageValidator($request);
      if($imageValidator->fails()){
        return response()->json([
          'message' => 'fail',
          'success' => false,
          'data'    => $imageValidator->errors(),
        ], 422);
      }

      $photo = $request->breed_photo;

      $time = time();
      $uploader = new ImageUploader($photo, $time);

      $updated_pet_breed['photo'] = $uploader->imageName();
    } else {
      $updated_pet_breed['photo'] = $old_breed->photo;
    }

    $validator = $this->validator($request, true);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.pet_breed_update_fail'),
        'success' => false,
        'data'    => $validator->errors(),
      ], 422);
    }

    $updated_pet_breed['updated_at'] = time();

    $pet_breed = PetBreed::find($request->id);
    $pet_breed->update($updated_pet_breed);

    return response()->json([
      'message' => trans('api.pet_breed_update_success'),
      'success' => true,
      'data'    => $pet_breed,
    ]);
  }

  public function delete(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'id' => 'required|exists:pet_breeds,id',
    ]);

    if($validator->fails()){
      return response()->json([
        'message' => trans('api.pet_breed_delete_validate_fail'),
        'success' => false,
        'data'    => $validator->error(),
      ], 422);
    }

    $pets = Pet::where('breed_id', $request->id)->get();
    if (count($pets) > 0) {
      return response()->json([
          'message' => trans('api.pet_breed_delete_fail'),
          'success' => false,
        ], 422);
    }

    $pet_breed = PetBreed::find($request->id);
    $pet_breed->delete();

    return response()->json([
      'message' => trans('api.pet_breed_delete_success'),
      'success' => true,
      'data'    => $pet_breed,
    ]);
  }

  public function deleteMultiple(Request $request)
  {
    $breed_IDs = $request->breed_IDs;

    $pets = Pet::whereIn('breed_id', $breed_IDs)->get();
    if (count($pets) > 0) {
      return response()->json([
        'message' => trans('api.pet_breeds_delete_fail'),
        'success' => false,
      ], 422);
    }

    $breeds_to_delete = PetBreed::whereIn('id', $breed_IDs)->delete();
    return response()->json([
      'message' => 'Delete multiple breed successful',
      'success' => true,
      'data'    => $breed_IDs,
    ]);
  }

  private function validator($request, $is_update = false)
  {
    $validator = Validator::make($request->all(), [
      'id' => $is_update ? 'required|exists:pet_breeds,id' : '',
      'name' => $is_update ? 'required' : 'required|unique:pet_breeds',
      'type_id' => 'required',
      'description' => '',
    ]);
    return $validator;
  }

  private function imageValidator($request)
  {
    $validator = Validator::make($request->all(), [
      'breed_photo' => 'image|mimes:jpg,jpeg,png,gif,svg|max:2048',
    ]);
    return $validator;
  }
}
