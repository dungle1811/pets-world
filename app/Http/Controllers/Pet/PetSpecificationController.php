<?php

namespace App\Http\Controllers\Pet;

use App\Model\PetSpecification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class PetSpecificationController extends Controller
{
  public function all(){
    return response()->json([
      'message' => trans('api.pet_specification_all_success'),
      'success' => true,
      'data'    => PetSpecification::all(),
    ]);
  }

  public function save(Request $request)
  {
    $validator = $this->validator($request, true);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.pet_specification_update_fail'),
        'success' => false,
        'data'    => $validator->errors(),
      ], 422);
    }

    $pet_specification = $request->specifications;
    PetSpecification::where('pet_type_id', $request->specifications[0]['pet_type_id'])->delete();
    $pet_specification = PetSpecification::insert($pet_specification);

    return response()->json([
      'message' => trans('api.pet_specification_create_success'),
      'success' => true,
      'data'    => $pet_specification,
    ]);
  }

  private function validator($request)
  {
    $validator = Validator::make($request->all(), [
      'specifications' => 'array',
      'specifications.*.name' => 'required',
      'specifications.*.unit_type_id' => 'required',
      'specifications.*.unit_id' => 'required',
      'specifications.*.pet_type_id' => 'required',
    ]);
    return $validator;
  }
}
