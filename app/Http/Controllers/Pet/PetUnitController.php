<?php

namespace App\Http\Controllers\Pet;

use App\Model\PetUnit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class PetUnitController extends Controller
{
  public function all(){
    return response()->json([
      'message' => trans('api.pet_unit_all_success'),
      'success' => true,
      'data'    => PetUnit::all(),
    ]);
  }
}
