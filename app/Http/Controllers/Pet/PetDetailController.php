<?php

namespace App\Http\Controllers\Pet;

use App\Model\PetDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;

class PetDetailController extends Controller
{
  public function all(){
    return response()->json([
      'message' => trans('api.pet_detail_all_success'),
      'success' => true,
      'data'    => PetDetail::all(),
    ]);
  }

  public function create(Request $request)
  {
    $validator = $this->validator($request);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.pet_detail_create_fail'),
        'success' => false,
        'data'    => $validator->errors(),
      ], 422);
    }

    $pet_detail = $request->all();
    $pet_detail = PetDetail::create($pet_detail);

    return response()->json([
      'message' => trans('api.pet_detail_create_success'),
      'success' => true,
      'data'    => $pet_detail,
    ]);
  }

  public function update(Request $request)
  {
    $validator = $this->validator($request, true);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.pet_detail_update_fail'),
        'success' => false,
        'data'    => $validator->errors(),
      ], 422);
    }

    $updated_pet_detail = $request->all();
    $updated_pet_detail['updated_at']=time();
    $pet_detail = PetDetail::find($request->id);
    $pet_detail->update($updated_pet_detail);

    return response()->json([
      'message' => trans('api.pet_detail_update_success'),
      'success' => true,
      'data'    => $pet_detail,
    ]);
  }
  public function delete(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'id' => 'required|exists:pet_details,id',
    ]);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.pet_detail_delete_fail'),
        'success' => false,
        'data'    => $validator->error(),
      ], 422);
    }
    $pet_detail = PetDetail::find($request->id);
    $pet_detail->delete();

    return response()->json([
      'message' => trans('api.pet_detail_delete_success'),
      'success' => true,
      'data'    => $pet_detail,
    ]);
  }

  private function validator($request, $is_update = false)
  {
    $validator = Validator::make($request->all(), [
      'id' => $is_update ? 'required|exists:pet_details,id' : '',
      'value' => 'required',
    ]);
    return $validator;
  }
}
