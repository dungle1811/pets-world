<?php

namespace App\Http\Controllers\Blog;

use App\Model\BlogTags;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class BlogTagsController extends Controller
{
  public function all(){
    return response()->json([
      'message' => trans('api.blog_tag_all_success'),
      'success' => true,
      'data'    => BlogTags::all(),
    ]);
  }

  public function create(Request $request)
  {
    $validator = $this->validator($request);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.blog_tag_create_fail'),
        'success' => false,
        'data'    => $validator->errors(),
      ], 422);
    }

    $blog_tag = $request->all();
    $blog_tag['created_at']=time();
    $blog_tag['updated_at']=time();
    $blog_tag = BlogTags::create($blog_tag);

    return response()->json([
      'message' => trans('api.blog_tag_create_success'),
      'success' => true,
      'data'    => $blog_tag,
    ]);
  }

  public function update(Request $request)
  {
    $validator = $this->validator($request, true);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.blog_tag_update_fail'),
        'success' => false,
        'data'    => $validator->errors(),
      ], 422);
    }

    $updated_blog_tag = $request->all();
    $updated_blog_tag['updated_at']=time();
    $blog_tag = BlogTags::find($request->id);
    $blog_tag->update($updated_blog_tag);

    return response()->json([
      'message' => trans('api.blog_tag_update_success'),
      'success' => true,
      'data'    => $blog_tag,
    ]);
  }
  public function delete(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'id' => 'required|exists:blog_tags,id',
    ]);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.blog_tag_delete_fail'),
        'success' => false,
        'data'    => $validator->error(),
      ], 422);
    }
    $blog_tag = BlogTags::find($request->id);
    $blog_tag->delete();

    return response()->json([
      'message' => trans('api.blog_tag_delete_success'),
      'success' => true,
      'data'    => $blog_tag,
    ]);
  }

  private function validator($request, $is_update = false)
  {
    $validator = Validator::make($request->all(), [
      'id' => $is_update ? 'required|exists:blog_tags,id' : '',
      'name' => 'required',
      'description' => '',
    ]);
    return $validator;
  }
}
