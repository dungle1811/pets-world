<?php

namespace App\Http\Controllers\Blog;

use App\Model\BlogStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class BlogStatusController extends Controller
{
  public function all(){
    return response()->json([
      'message' => trans('api.blog_status_all_success'),
      'success' => true,
      'data'    => BlogStatus::all(),
    ]);
  }

  public function create(Request $request)
  {
    $validator = $this->validator($request);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.blog_status_create_fail'),
        'success' => false,
        'data'    => $validator->errors(),
      ], 422);
    }

    $blog_status = $request->all();
    $blog_status['created_at']=time();
    $blog_status['updated_at']=time();
    $blog_status = BlogStatus::create($blog_status);

    return response()->json([
      'message' => trans('api.blog_status_create_success'),
      'success' => true,
      'data'    => $blog_status,
    ]);
  }

  public function update(Request $request)
  {
    $validator = $this->validator($request, true);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.blog_status_update_fail'),
        'success' => false,
        'data'    => $validator->errors(),
      ], 422);
    }

    $updated_blog_status = $request->all();
    $updated_blog_status['updated_at']=time();
    $blog_status = BlogStatus::find($request->id);
    $blog_status->update($updated_blog_status);

    return response()->json([
      'message' => trans('api.blog_status_update_success'),
      'success' => true,
      'data'    => $blog_status,
    ]);
  }
  public function delete(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'id' => 'required|exists:blog_status,id',
    ]);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.blog_status_delete_fail'),
        'success' => false,
        'data'    => $validator->error(),
      ], 422);
    }
    $blog_status = BlogStatus::find($request->id);
    $blog_status->delete();

    return response()->json([
      'message' => trans('api.blog_status_delete_success'),
      'success' => true,
      'data'    => $blog_status,
    ]);
  }

  private function validator($request, $is_update = false)
  {
    $validator = Validator::make($request->all(), [
      'id' => $is_update ? 'required|exists:blog_status,id' : '',
      'name' => 'required',
      'description' => '',
    ]);
    return $validator;
  }
}
