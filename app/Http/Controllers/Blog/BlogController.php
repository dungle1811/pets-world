<?php

namespace App\Http\Controllers\Blog;

use App\Model\Blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class BlogController extends Controller
{
  public function all(){
    return response()->json([
      'message' => trans('api.blog_all_success'),
      'success' => true,
      'data'    => Blog::all(),
    ]);
  }

  public function create(Request $request)
  {
    $validator = $this->validator($request);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.blog_create_fail'),
        'success' => false,
        'data'    => $validator->errors(),
      ], 422);
    }

    $blog= $request->all();
    $blog['created_at']=time();
    $blog['updated_at']=time();
    $blog = Blog::create($blog);

    return response()->json([
      'message' => trans('api.blog_create_success'),
      'success' => true,
      'data'    => $blog,
    ]);
  }

  public function update(Request $request)
  {
    $validator = $this->validator($request, true);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.blog_update_fail'),
        'success' => false,
        'data'    => $validator->errors(),
      ], 422);
    }

    $updated_blog = $request->all();
    $updated_blog['updated_at']=time();
    $blog = Blog::find($request->id);
    $blog->update($update_blog);

    return response()->json([
      'message' => trans('api.blog_update_success'),
      'success' => true,
      'data'    => $blog,
    ]);
  }
  public function delete(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'id' => 'required|exists:blogs,id',
    ]);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.blog_delete_fail'),
        'success' => false,
        'data'    => $validator->error(),
      ], 422);
    }
    $blog = Blog::find($request->id);
    $blog->delete();

    return response()->json([
      'message' => trans('api.blog_delete_success'),
      'success' => true,
      'data'    => $blog,
    ]);
  }

  private function validator($request, $is_update = false)
  {
    $validator = Validator::make($request->all(), [
      'id' => $is_update ? 'required|exists:blogs,id' : '',
      'title' => 'required',
      'user_id' => 'required',
      'status_id' => 'required',
    ]);
    return $validator;
  }
}
