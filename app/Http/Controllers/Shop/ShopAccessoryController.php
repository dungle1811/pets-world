<?php

namespace App\Http\Controllers\Shop;

use App\Model\ShopAccessory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;

class ShopAccessoryController extends Controller
{
  public function all(){
    return response()->json([
      'message' => trans('api.shop_accessory_all_success'),
      'success' => true,
      'data'    => ShopAccessory::all(),
    ]);
  }

  public function create(Request $request)
  {
    $validator = $this->validator($request);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.shop_accessory_create_fail'),
        'success' => false,
        'data'    => $validator->errors(),
      ], 422);
    }

    $shop_accessory= $request->all();
    $shop_accessory['created_at']=time();
    $shop_accessory['updated_at']=time();
    $shop_accessory['shop_id']= Auth::guard('api')->user()->current_shop_id;
    $shop_accessory = ShopAccessory::create($shop_accessory);

    return response()->json([
      'message' => trans('api.shop_accessory_create_success'),
      'success' => true,
      'data'    => $shop_accessory,
    ]);
  }

  public function update(Request $request)
  {
    $validator = $this->validator($request, true);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.shop_accessory_update_fail'),
        'success' => false,
        'data'    => $validator->errors(),
      ], 422);
    }

    $updated_shop_accessory = $request->all();
    $updated_shop_accessory['updated_at']=time();
    $shop_accessory = ShopAccessory::find($request->id);
    $shop_accessory->update($updated_shop_accessory);

    return response()->json([
      'message' => trans('api.shop_accessory_update_success'),
      'success' => true,
      'data'    => $shop_accessory,
    ]);
  }
  public function delete(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'id' => 'required|exists:shop_accessories,id',
    ]);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.shop_accessory_delete_fail'),
        'success' => false,
        'data'    => $validator->error(),
      ], 422);
    }
    $shop_accessory = ShopAccessory::find($request->id);
    $shop_accessory->delete();

    return response()->json([
      'message' => trans('api.shop_accessory_delete_success'),
      'success' => true,
      'data'    => $shop_accessory,
    ]);
  }

  private function validator($request, $is_update = false)
  {
    $validator = Validator::make($request->all(), [
      'id' => $is_update ? 'required|exists:shop_accessories,id' : '',
      'name' => 'required',
      'unit' => 'required',
      'quantity' => 'required',
      'price' => 'required',
      'origin_country' => 'required',
      'photo' => '',
      'description' => '',
    ]);
    return $validator;
  }
}
