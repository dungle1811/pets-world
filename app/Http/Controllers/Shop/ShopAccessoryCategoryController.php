<?php

namespace App\Http\Controllers\Shop;

use App\Model\ShopAccessoryCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;

class ShopAccessoryCategoryController extends Controller
{
  public function all(){
    return response()->json([
      'message' => trans('api.shop_accessory_category_all_success'),
      'success' => true,
      'data'    => ShopAccessoryCategory::all(),
    ]);
  }

  public function create(Request $request)
  {
    $validator = $this->validator($request);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.shop_accessory_category_create_fail'),
        'success' => false,
        'data'    => $validator->errors(),
      ], 422);
    }

    $shop_accessory_category= $request->all();
    $shop_accessory_category['shop_id']= Auth::guard('api')->user()->current_shop_id;
    $shop_accessory_category = ShopAccessoryCategory::create($shop_accessory_category);

    return response()->json([
      'message' => trans('api.shop_accessory_category_create_success'),
      'success' => true,
      'data'    => $shop_accessory_category,
    ]);
  }

  public function update(Request $request)
  {
    $validator = $this->validator($request, true);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.shop_accessory_category_update_fail'),
        'success' => false,
        'data'    => $validator->errors(),
      ], 422);
    }

    $updated_shop_accessory_category = $request->all();
    $updated_shop_accessory_category['updated_at']=time();
    $shop_accessory_category = ShopAccessoryCategory::find($request->id);
    $shop_accessory_category->update($updated_shop_accessory_category);

    return response()->json([
      'message' => trans('api.shop_accessory_category_update_success'),
      'success' => true,
      'data'    => $shop_accessory_category,
    ]);
  }
  public function delete(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'id' => 'required|exists:shop_accessory_categories,id',
    ]);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.shop_accessory_category_delete_fail'),
        'success' => false,
        'data'    => $validator->error(),
      ], 422);
    }
    $shop_accessory_category = ShopAccessoryCategory::find($request->id);
    $shop_accessory_category->delete();

    return response()->json([
      'message' => trans('api.shop_accessory_category_delete_success'),
      'success' => true,
      'data'    => $shop_accessory_category,
    ]);
  }

  private function validator($request, $is_update = false)
  {
    $validator = Validator::make($request->all(), [
      'id' => $is_update ? 'required|exists:shop_accessory_categories,id' : '',
      'name' => 'required',
      'description' => '',
    ]);
    return $validator;
  }
}
