<?php

namespace App\Http\Controllers\Shop;

use App\Model\ShopStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;

class ShopStatusController extends Controller
{
  public function all(){
    return response()->json([
      'message' => trans('api.shop_status_all_success'),
      'success' => true,
      'data'    => ShopStatus::all(),
    ]);
  }

  public function create(Request $request)
  {
    $validator = $this->validator($request);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.shop_status_create_fail'),
        'success' => false,
        'data'    => $validator->errors(),
      ], 422);
    }

    $shop_status= $request->all();
    $shop_status['shop_id']= Auth::guard('api')->user()->current_shop_id;
    $shop_status = ShopStatus::create($shop_status);

    return response()->json([
      'message' => trans('api.shop_status_create_success'),
      'success' => true,
      'data'    => $shop_status,
    ]);
  }

  public function update(Request $request)
  {
    $validator = $this->validator($request, true);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.shop_status_update_fail'),
        'success' => false,
        'data'    => $validator->errors(),
      ], 422);
    }

    $updated_shop_status = $request->all();
    $updated_shop_status['updated_at']=time();
    $shop_status = ShopStatus::find($request->id);
    $shop_status->update($updated_shop_status);

    return response()->json([
      'message' => trans('api.shop_status_update_success'),
      'success' => true,
      'data'    => $shop_status,
    ]);
  }
  public function delete(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'id' => 'required|exists:shop_status,id',
    ]);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.shop_status_delete_fail'),
        'success' => false,
        'data'    => $validator->error(),
      ], 422);
    }
    $shop_status = ShopStatus::find($request->id);
    $shop_status->delete();

    return response()->json([
      'message' => trans('api.shop_status_delete_success'),
      'success' => true,
      'data'    => $shop_status,
    ]);
  }

  private function validator($request, $is_update = false)
  {
    $validator = Validator::make($request->all(), [
      'id' => $is_update ? 'required|exists:shop_status,id' : '',
      'name' => 'required',
      'description' => '',
    ]);
    return $validator;
  }
}
