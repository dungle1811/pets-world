<?php

namespace App\Http\Controllers\Shop;

use App\Model\ShopService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;

class ShopServiceController extends Controller
{
  public function all(){
    return response()->json([
      'message' => trans('api.shop_service_all_success'),
      'success' => true,
      'data'    => ShopService::all(),
    ]);
  }

  public function create(Request $request)
  {
    $validator = $this->validator($request);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.shop_service_create_fail'),
        'success' => false,
        'data'    => $validator->errors(),
      ], 422);
    }

    $shop_service= $request->all();
    $shop_service['shop_id']= Auth::guard('api')->user()->current_shop_id;
    $shop_service = ShopService::create($shop_service);

    return response()->json([
      'message' => trans('api.shop_service_create_success'),
      'success' => true,
      'data'    => $shop_service,
    ]);
  }

  public function update(Request $request)
  {
    $validator = $this->validator($request, true);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.shop_service_update_fail'),
        'success' => false,
        'data'    => $validator->errors(),
      ], 422);
    }

    $updated_shop_service = $request->all();
    $updated_shop_service['updated_at']=time();
    $shop_service = ShopService::find($request->id);
    $shop_service->update($updated_shop_service);

    return response()->json([
      'message' => trans('api.shop_service_update_success'),
      'success' => true,
      'data'    => $shop_service,
    ]);
  }
  public function delete(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'id' => 'required|exists:shop_services,id',
    ]);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.shop_service_delete_fail'),
        'success' => false,
        'data'    => $validator->error(),
      ], 422);
    }
    $shop_service = ShopService::find($request->id);
    $shop_service->delete();

    return response()->json([
      'message' => trans('api.shop_service_delete_success'),
      'success' => true,
      'data'    => $shop_service,
    ]);
  }

  private function validator($request, $is_update = false)
  {
    $validator = Validator::make($request->all(), [
      'id' => $is_update ? 'required|exists:shop_services,id' : '',
      'name' => 'required',
      'price' => 'required',
      'photo' => '',
      'description' => '',
    ]);
    return $validator;
  }
}
