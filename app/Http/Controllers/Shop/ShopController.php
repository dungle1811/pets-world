<?php

namespace App\Http\Controllers\Shop;

use App\Model\Shop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class ShopController extends Controller
{
  public function all(){
    return response()->json([
      'message' => trans('api.shop_all_success'),
      'success' => true,
      'data'    => Shop::with('owner')->get(),
    ]);
  }

  public function create(Request $request)
  {
    $validator = $this->validator($request);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.shop_create_fail'),
        'success' => false,
        'data'    => $validator->errors(),
      ], 422);
    }

    $shop= $request->all();
    $shop['created_at']=time();
    $shop['updated_at']=time();
    $shop = Shop::create($shop);

    return response()->json([
      'message' => trans('api.shop_create_success'),
      'success' => true,
      'data'    => $shop,
    ]);
  }

  public function update(Request $request)
  {
    $validator = $this->validator($request, true);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.shop_update_fail'),
        'success' => false,
        'data'    => $validator->errors(),
      ], 422);
    }

    $updated_shop = $request->all();
    $updated_shop['updated_at']=time();
    $shop = Shop::find($request->id);
    $shop->update($updated_shop);

    return response()->json([
      'message' => trans('api.shop_update_success'),
      'success' => true,
      'data'    => $shop,
    ]);
  }
  public function delete(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'id' => 'required|exists:shops,id',
    ]);
    if($validator->fails()){
      return response()->json([
        'message' => trans('api.shop_delete_fail'),
        'success' => false,
        'data'    => $validator->error(),
      ], 422);
    }
    $shop = Shop::find($request->id);
    $shop->delete();

    return response()->json([
      'message' => trans('api.shop_delete_success'),
      'success' => true,
      'data'    => $shop,
    ]);
  }

  private function validator($request, $is_update = false)
  {
    $validator = Validator::make($request->all(), [
      'id' => $is_update ? 'required|exists:shops,id' : '',
      'name' => 'required',
      'country' => 'required',
      'city' => 'required',
      'address' => 'required',
      'phone' => 'required',
      'owner_id' => 'required',
      'photo' => '',
      'description' => '',
    ]);
    return $validator;
  }
}
