<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pet extends Model
{
    public $timestamps = false;

    protected $primaryKey = 'id';

    protected $table = 'pets';

    protected $fillable = [
      "name", "description", "sex", "height", "weight", "price",
      "photo_landscape", "photo_square",
      "breed_id", "type_id", "shop_id",
      "created_at", "updated_at"
    ];

    protected $hidden = [
      "shop_id",
    ];

    public function pet_breed()
    {
      return $this->belongsTo('App\Model\PetBreed', 'breed_id');
    }

    public function pet_status()
    {
      return $this->belongsToMany('App\Model\PetStatus', 'pets_status', 'pets_id', 'status_id');
    }

    public function pet_type()
    {
        return $this->belongsTo('App\Model\PetType', 'type_id');
    }

    public function shop()
    {
        return $this->belongsTo('App\Model\Shop', 'shop_id');
    }

    public function details()
    {
        return $this->hasMany('App\Model\PetDetail', 'pet_id');
    }
}
