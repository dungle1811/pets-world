<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PetDetail extends Model
{
  public $timestamps = false;

  protected $primaryKey = 'id';

  protected $table = 'pet_details';

  protected $fillable = [
     "value",
     "pet_id", "specification_id",
  ];

  public function specification()
  {
    return $this->belongsToMany('App\Model\PetSpecification', 'specification_id');
  }

  public function pet()
  {
    return $this->belongsTo('App\Model\Pet', 'pet_id');
  }
}
