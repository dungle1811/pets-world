<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BlogStatus extends Model
{
  public $timestamps = false;

  protected $primaryKey = 'id';

  protected $table = 'blog_status';

  protected $fillable = [
      "name", "description",
      "created_at", "updated_at",
  ];
}
