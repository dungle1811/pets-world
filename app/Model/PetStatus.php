<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PetStatus extends Model
{
    public $timestamps = false;

    protected $primaryKey = 'id';

    protected $table = 'pet_status';

    protected $fillable = [
         "name", "description",
         "shop_id",
    ];

    protected $hidden = [
        "shop_id",
    ];
    public function pets(){
      return $this->belongsToMany('App\Model\Pet', 'pets_status', 'pet_id', 'status_id');
    }

}
