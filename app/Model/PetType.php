<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PetType extends Model
{
    public $timestamps = false;

    protected $primaryKey = 'id';

    protected $table = 'pet_types';

    protected $fillable = [
       "name", "description", "shop_id",
    ];

    public function pets()
    {
        return $this->hasMany('App\Model\Pet', 'type_id');
    }

    public function type_breeds()
    {
        return $this->hasMany('App\Model\PetBreed', 'type_id');
    }

    public function specifications()
    {
        return $this->hasMany('App\Model\PetSpecification', 'pet_type_id');
    }

    public function detail()
    {
        return $this->hasMany('App\Model\PetDetail', 'type_detail_id');
    }
}
