<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
  public $timestamps = false;

  protected $primaryKey = 'id';

  protected $table = 'blogs';

  protected $fillable = [
      "title",
      "user_id", "status_id",
      "created_at", "updated_at",
  ];
}
