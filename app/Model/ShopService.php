<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ShopService extends Model
{
  public $timestamps = false;

  protected $primaryKey = 'id';

  protected $table = 'shop_services';

  protected $fillable = [
    "name", "price", "photo", "description",
    "shop_id",
  ];

  protected $hidden = [
    "shop_id",
  ];

  public function shop(){
    return $this->hasMany('App\Model\Shop', 'shop_id');
  }
}
