<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PetSpecification extends Model
{
  public $timestamps = false;

  protected $primaryKey = 'id';

  protected $table = 'pet_specifications';

  protected $fillable = [
     "name",
     "pet_type_id", "unit_type_id", "unit_id",
  ];

  public function unit_type()
  {
    return $this->belongsTo('App\Model\PetUnitType', 'unit_type_id');
  }

  public function unit()
  {
    return $this->belongsTo('App\Model\PetUnit', 'unit_id');
  }

  public function details()
  {
    return $this->hasMany('App\Model\PetDetail', 'specification_id');
  }

  public function type()
    {
      return $this->belongsTo('App\Model\PetType', 'pet_type_id');
    }
}
