<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PetBreed extends Model
{
    public $timestamps = false;

    protected $primaryKey = 'id';

    protected $table = 'pet_breeds';

    protected $fillable = [
      "name", "description",
      "type_id", "shop_id", "photo",
    ];

    protected $hidden = [
      "shop_id",
    ];

    public function pets(){
        return $this->hasMany('App\Model\Pet', 'type_id');
    }

    public function pet_type()
    {
      return $this->belongsTo('App\Model\PetType', 'type_id');
    }
}
