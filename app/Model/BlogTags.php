<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BlogTags extends Model
{
  public $timestamps = false;

  protected $primaryKey = 'id';

  protected $table = 'blog_tags';

  protected $fillable = [
      "name", "description",
      "created_at", "updated_at",
  ];
}
