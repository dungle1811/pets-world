<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PetUnitType extends Model
{
  public $timestamps = false;

  protected $primaryKey = 'id';

  protected $table = 'pet_unit_types';

  protected $fillable = [
     "name",
  ];
  public function units()
  {
    return $this->hasMany('App\Model\PetUnit', 'type_id');
  }
}
