<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ShopStatus extends Model
{
  public $timestamps = false;

  protected $primaryKey = 'id';

  protected $table = 'shop_status';

  protected $fillable = [
    "name", "description",
    "shop_id",
  ];

  protected $hidden = [
    "shop_id",
  ];

  public function shop(){
    return $this->hasMany('App\Model\Shop', 'shop_id');
  }
}
