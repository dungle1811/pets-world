<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ShopAccessory extends Model
{
  public $timestamps = false;

  protected $primaryKey = 'id';

  protected $table = 'shop_accessories';

  protected $fillable = [
    "name", "description", "unit", "quantity", "price", "origin_country", "photo", "produced_at", "exprired_at",
    "category_id", "shop_id",
    "created_at", "updated_at", "produced_at", "exprired_at",
  ];

  protected $hidden = [
    "shop_id",
  ];

  public function shop()
  {
      return $this->hasMany('App\Model\Shop', 'shop_id');
  }
}
