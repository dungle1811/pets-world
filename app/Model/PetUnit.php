<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PetUnit extends Model
{
  public $timestamps = false;

  protected $primaryKey = 'id';

  protected $table = 'pet_units';

  protected $fillable = [
     "name", "short_name", "type_id"
  ];

  public function pet_type()
  {
    return $this->belongsTo('App\Model\PetType', 'type_id');
  }

  public function specification_units()
  {
    return $this->belongsTo('App\Model\PetSpecification', 'unit_id');
  }
}
