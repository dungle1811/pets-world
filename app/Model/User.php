<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $primaryKey = 'id';

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'country', 'city', 'address', 'phone', 'photo', 'email', 'password', 'authority_id', 'created_at', 'updated_at', 'api_token', 'is_confirmed',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token', 'confirmation_token',
    ];

    public $timestamps = false;

    public function shops()
    {
        return $this->hasMany('App\Model\Shop', 'owner_id');
    }

    public function current_shop()
    {
        return $this->belongsTo('App\Model\Shop', 'current_shop_id');
    }

}
