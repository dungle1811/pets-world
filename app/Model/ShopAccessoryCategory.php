<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ShopAccessoryCategory extends Model
{
  public $timestamps = false;

  protected $primaryKey = 'id';

  protected $table = 'shop_accessory_categories';

  protected $fillable = [
    "name", "description",
    "shop_id",
  ];

  protected $hidden = [
    "shop_id",
  ];
}
