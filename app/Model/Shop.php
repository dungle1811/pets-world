<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
  public $timestamps = false;

  protected $primaryKey = 'id';

  protected $table = 'shops';

  protected $fillable = [
      "name", "description", "country", "city", "address", "phone", "photo",
      "owner_id", "status_id",
      "created_at", "updated_id",
  ];

  public function pets()
  {
      return $this->hasMany('App\Model\Pet', 'type_id');
  }

  public function shop()
  {
      return $this->belongsTo('App\Model\Shop', 'shop_id');
  }

  public function owner()
  {
      return $this->belongsTo('App\Model\User', 'owner_id');
  }
}
