<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'email' => 'Email',
    'password' => 'Password',
    'password_confirmation' => 'Password confirmation',
    'login' => 'Login',
    'logout' => 'Logout',
    'register' => 'Register',
    'forgot_password' => 'Forgot your password',
    'register_membership' => 'Register a new membership',
    'already_membership' => 'Already a membership',
    'confirmation_title' => 'Confirm your email',
    'confirmation_content' => 'A confirmation link has been sent to your email address, please check your inbox or spam box.',
    'confirm_successful' => 'Congratulation, your account has been successfully registered',
    'confirm_fail' => 'Something went wrong',
    'email_not_confirmed' => 'Your email have not yet confirmed.'
];
