<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>{{ trans('site.project_name') }} &bullet; @yield('title')</title>
        <link rel="stylesheet" href="/bundled/css/auth.css">
        @yield('styles')
    </head>
    <body>
        @yield('body')
        @yield('scripts')
    </body>
</html>
