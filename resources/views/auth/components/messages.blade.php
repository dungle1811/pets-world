@php
    $messages = Session::pull('messages');
@endphp
@if(!empty($messages))
    @foreach ($messages as $message)
        <div class="alert {{ isset($message[1]) && $message[1] == 'info' ? 'alert-info' : 'alert-danger' }} mt-2">{{ $message[0] }}</div>
    @endforeach
@endif
