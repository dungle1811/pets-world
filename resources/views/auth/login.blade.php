@extends('auth.master')

@section('title', trans('auth.login'))

@section('body')
    <div class="background">

        <div class="container d-flex">
            <form class="card mx-auto my-auto" action="{{ route('view.auth.login') }}" method="POST">
                <div class="card-block">
                    {{ csrf_field() }}
                    @php
                        $errors = Session::pull('errors');
                    @endphp
                    @include('auth.components.messages')
                    <div class="form-group">
                        <label for="email">{{ trans('auth.email') }}</label>
                        <input type="text" name="email" class="form-control" value="{{ old('email') }}" placeholder="{{ trans('auth.email') }}">
                        @if(!empty($errors) && $errors->has('email'))
                            @foreach ($errors->get('email') as $error)
                                <div class="alert alert-danger mt-2">{{ $error }}</div>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="password">{{ trans('auth.password') }}</label>
                        <input type="password" name="password" class="form-control" placeholder="{{ trans('auth.password') }}">
                        @if(!empty($errors) && $errors->has('password'))
                            @foreach ($errors->get('password') as $error)
                                <div class="alert alert-danger mt-2">{{ $error }}</div>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group ">
                        <button class="btn btn-primary btn-block">Login</button>
                        <a href="{{ route('view.auth.register') }}" class="d-block">{{ trans('auth.register_membership') }}</a>
                        <a href="{{ route('view.auth.register') }}" class="d-block">{{ trans('auth.forgot_password') }}</a>
                        <a href="{{ route('view.home.index') }}" class="d-block">Back to Home</a>
                    </div>
                </div>
            </form>
        </div>
        <footer>
            @include('homepage/components/footer');
        </footer>
    </div>
@endsection
