<ul class="sidebar-menu">
    <li class="sidebar-item nav-item">
        <a onclick="window.location = '/'" class="nav-link">
            <i class="fa fa-home"></i>
            <span class="title">@lang('site.homepage')</span>
        </a>
    </li>
    <li class="sidebar-item nav-item">
        <a ui-sref="dashboard" ui-sref-active="active" class="nav-link">
            <i class="fa fa-tachometer"></i>
            <span>@lang('site.dashboard')</span>
        </a>
    </li>
    <li class="sidebar-item nav-item">
        <a ui-sref="pet.list" ui-sref-active="active" class="nav-link">
            <i class="fa fa-paw"></i>
            <span>@lang('site.pets')</span>
            <span class="arrow"><i class="fa fa-angle-down"></i></span>
        </a>
        <ul class="sub-menu">
            <li>
                <a ui-sref="pet.types" ui-sref-active="active" class="nav-link">
                    <span class="title">@lang('site.pet_types_title')</span>
                </a>
            </li>
            <li>
                <a ui-sref="pet.breeds" ui-sref-active="active" class="nav-link">
                    <span class="title">@lang('site.pet_breeds_title')</span>
                </a>
            </li>
            <li>
                <a ui-sref="pet.status" ui-sref-active="active" class="nav-link">
                    <span class="title">@lang('site.pet_status_title')</span>
                </a>
            </li>
            <li>
                <a ui-sref="pet.create" ui-sref-active="active" class="nav-link">
                    <span class="title">@lang('site.pet_create')</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="sidebar-item nav-item">
        <a ui-sref="user.profile.info" ui-sref-active="active" class="nav-link">
            <i class="fa fa-user"></i>
            <span>@lang('site.user_profile')</span>
            <span class="arrow"><i class="fa fa-angle-down"></i></span>
        </a>
        <ul class="sub-menu">
            <li>
                <a ui-sref="user.profile.avatar" ui-sref-active="active" class="nav-link">
                    <span class="title">@lang('site.change_avatar')</span>
                </a>
            </li>
            <li>
                <a ui-sref="user.profile.password" ui-sref-active="active" class="nav-link">
                    <span class="title">@lang('site.change_password')</span>
                </a>
            </li>
        </ul>
    </li>
</ul>
