<div class="logo">
        <a href="#" class="logo-text">
            @lang('site.pet')
                <i class="fa fa-paw mx-1" style="color: #90caf9;"></i>
            @lang('site.world')
        </a>
    <div class="menu-area mr-4 ">
        <label class="sidebar-toggler" for="menu">
            <i class="fa fa-bars"></i>
        </label>
    </div>
</div>
<div class="action">

</div>
<div class="nav navbar">
    <ul class="list-icons">
        <li><i class="fa fa-bell"></i></li>
        <li><i class="fa fa-envelope-open-o"></i></li>
        <li><i class="fa fa-calendar"></i></li>
        <li>
            <span ng-bind="Auth.user.email"
                ng-if="!(Auth.user.firstname || Auth.user.lastname)"></span>
            <span ng-bind="Auth.user.firstname + ' ' + Auth.user.lastname"
                ng-if="Auth.user.firstname || Auth.user.lastname"></span>
        </li>
        <li>
            <img src="image/default_assets/User.jpg" alt="@lang('site.world')">
        </li>
        <li><a href="{{ route('view.auth.logout') }}"><i class="fa fa-sign-out" style="font-size: 18px;"></i></a></li>
    </ul>
</div>
