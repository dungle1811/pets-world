<div class="profile-content">
    <div class="col-xs-12 col-sm-12 col-md-4 no-spacing box-1 px-0 py-0">
        <div class="d-flex flex-column profile-status pb-3 background-white px-2 py-2">
            <div class="d-flex profile-image py-2">
                <img src="image/default_assets/User.jpg" class="m-auto" alt="user-profile">
            </div>
            <div class="d-flex profile-name font">
                <span class="mx-auto font-size-20">
                    <span ng-bind="Auth.user.email"
                        ng-if="!(Auth.user.firstname || Auth.user.lastname)"></span>
                    <span ng-bind="Auth.user.firstname + ' ' + Auth.user.lastname"
                        ng-if="Auth.user.firstname || Auth.user.lastname"></span>
                </span>
            </div>
            <div class="d-flex profile-usertitle font">
                <span class="mx-auto font-user-16">
                    @lang('site.title')
                </span>
            </div>
            <div class="d-flex flex-column font pb-3 pb-md-0">
                <div class="profile-about font-weight-700 mx-2 py-2">
                    @lang('site.about') <span ng-bind="Auth.user.firstname || '@lang('site.me')'"></span>
                </div>
                <div class="profile-about-text mx-2 font-14 pb-2">
                    Lorem ipsum dolor sit amet diam nonummy nibh dolore.
                </div>
                <div class="row py-2">
                    <div class="col-2 offset-3 icon font profile-facebook text-center px-0">
                        <i class="fa fa-facebook font-size-20 font-iconcolor"></i>

                    </div>
                    <div class="col-2 icon font profile-twitter text-center px-0">
                        <i class="fa fa-twitter font-size-20 font-iconcolor"></i>
                    </div>
                    <div class="col-2 icon font profile-google text-center px-0">
                        <i class="fa fa-google font-size-20 font-iconcolor"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8 px-3 background-white ml-2 py-2 pt-3 box-2">
        <div class="d-flex profile-details px-3">
            <div class="profile-details-header font py-2 hidden-sm-down">
                <span class="font-size-20">@lang('site.user_profile')</span>
            </div>
            <div class="nav nav-tabs d-flex ml-0 ml-md-auto">
                <ul class="d-flex nav-list my-auto">
                    <li class="nav-link px-3" ui-sref="user.profile.info" ui-sref-active="active">@lang('site.information')</li>
                    <li class="nav-link px-3" ui-sref="user.profile.avatar" ui-sref-active="active">@lang('site.change_avatar')</li>
                    <li class="nav-link px-3" ui-sref="user.profile.password" ui-sref-active="active">@lang('site.change_password')</li>
                </ul>
            </div>
        </div>
        <div class="d-flex profile-details-body py-3 px-3">
            <ui-view class="w-100 pt-4"></ui-view>
        </div>
    </div>
</div>
