<form class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 no-spacing d-flex flex-column mx-auto">
    <div class="form-group">
        <label for="email">Email</label>
        <input type="text" class="form-control" id="email" placeholder="Steve" ng-model="user.email" disabled>
    </div>
    <div class="form-group">
        <label for="firstname">First name</label>
        <input type="text" class="form-control" id="firstname" placeholder="Steve" ng-model="user.firstname">
    </div>
    <div class="form-group">
        <label for="lastname">Last name</label>
        <input type="text" class="form-control" id="lastname" placeholder="Anderson" ng-model="user.lastname">
    </div>
    <div class="form-group">
        <label for="phone">Phone Number</label>
        <input type="text" class="form-control" id="phone" placeholder="0121-944-5665" ng-model="user.phone">
    </div>
    <div class="form-group">
        <label for="address">Addressn</label>
        <input type="text" class="form-control" id="address" placeholder="0121-944-5665" ng-model="user.address">
    </div>
    <div class="form-group">
        <label for="country">Country</label>
        <input type="text" class="form-control" id="country" placeholder="0121-944-5665" ng-model="user.country">
    </div>
    <div class="form-group">
        <label for="city">City</label>
        <input type="text" class="form-control" id="city" placeholder="0121-944-5665" ng-model="user.city">
    </div>

    {{-- <div class="form-group">
        <label for="interest">Interests</label>
        <input type="text" class="form-control" id="interest" placeholder="Music, artistic, sports" ng-model="user.">
    </div>
    <div class="form-group">
        <label for="job">Occupation</label>
        <input type="text" class="form-control" id="job" placeholder="Developer" ng-model="user.">
    </div>
    <div class="form-group">
        <label for="facebook">Facebook</label>
        <input type="text" class="form-control" id="facebook" placeholder="Link" ng-model="user.">
    </div>
    <div class="form-group">
        <label for="twitter">Twitter</label>
        <input type="text" class="form-control" id="twitter" placeholder="Link" ng-model="user.">
    </div>
    <div class="form-group">
        <label for="google">Google</label>
        <input type="text" class="form-control" id="google" placeholder="Link" ng-model="user.">
    </div>
    <div class="form-group">
        <label for="desc">Description</label>
        <input type="text" class="form-control" id="desc" placeholder="About self" ng-model="user.">
    </div> --}}
    <div class="group-button ml-auto pt-4">
        <button ng-click="updateInfo()" class="btn btn-primary">Save</button>
    </div>
</form>
