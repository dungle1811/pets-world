<form class="col-12 d-flex flex-column">
    <div class="col-6 d-flex flex-column no-spacing mx-auto justify-content-center">
        <div class="layout-description">
            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
        </div>
        <div class="form-group photo-landscape mt-4 mx-auto">
            <label>@lang('site.image')</label>
            <file-input model="breed.photo" model-name="breed"></file-input>
        </div>
        <div class="group-button ml-auto pt-4">
            <button type="submit" class="btn btn-secondary">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>
