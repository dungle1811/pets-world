<form class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 d-flex flex-column mx-auto no-spacing">
    <div class="form-group">
        <label for="oldpassword">Current Password</label>
        <input type="password" class="form-control" id="oldpassword">
    </div>
    <div class="form-group">
        <label for="newpassword">New Password</label>
        <input type="password" class="form-control" id="newpassword">
    </div>
    <div class="form-group">
        <label for="repassword">Re-enter Password</label>
        <input type="password" class="form-control" id="repassword">
    </div>
    <div class="group-button ml-auto pt-4">
        <button type="submit" class="btn btn-secondary">Cancel</button>
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>
