<div class="list">
    <div class="list-group">
        <div class="list-group-item media"
                ng-repeat="breed in breeds | orderBy: '-id' | filter: searchText | filter: typeOption">
            <div class="check-form d-flex mr-1">
                <input type="checkbox"
                        id="breed-[[breed.id]]"
                        class="check-form-input check-model"
                        ng-model="breed.selected"
                        ng-change="changeCheckedOption()"
                        value="[[breed.id]]"
                        name="selectedBreeds[]">
                <label for="breed-[[breed.id]]">
                    <img ng-src="[[breed.photo != null ? '/upload/images/small/' + breed.photo : '/image/default_assets/presentation/default_pet.jpg']]">
                    <span class="fa fa-check"></span>
                </label>
            </div>
            <div class="media-body">
                <div class="row">
                    <div class="info col-12">
                        <p ng-bind="breed.name" class="name"></p>
                        <p ng-bind="breed.description" class="description"></p>
                    </div>
                    <div class="features-area">
                        <div class="features">
                            <label ng-click="setUpdate(breed)"><i class="fa fa-pencil" aria-hidden="true"></i></label>
                            <label ng-click="setDelete(breed)" data-toggle="modal" data-target="#deleteModal">
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
