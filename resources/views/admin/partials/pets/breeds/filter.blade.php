<div class="form-group filter-area d-flex">
    <input type="text" class="form-control search-input" id="search-input"
                placeholder="@lang('site.search_placeholder')" ng-model="searchText">
    <select class="form-control option-input ml-1"
            ng-options="type.name as type.name for type in types track by type.name"
            ng-model="typeOption">
        <option value="">[[default_option]]</option>
    </select>
    <button class="btn btn-secondary btn-sm ml-1 reset-filter" ng-click="resetFilter()">@lang('site.reset')</button>
</div>
