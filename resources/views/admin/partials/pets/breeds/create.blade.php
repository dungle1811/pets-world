<h5 class="title">@lang('site.breeds_management_title')</h5>
<p class="layout-description">
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur commodi eum quae, ipsum modi odio nulla, aliquam blanditiis dolorem.
</p>
<ng-form name="form.submitForm">
    <div ng-class="{
            'has-danger': form.submitForm.name.$invalid && form.submitForm.name.$touched || isExistedName,
            'has-success': form.submitForm.name.$valid,
        }"
        class="form-group">
        <label class="form-control-label" for="name">@lang('site.name')</label>
        <input type="text" id="name" class="form-control form-control-success form-control-danger"
                ng-model="breed.name"
                ng-change="resetNameError()"
                placeholder="@lang('site.breed_name_placeholder')"
                name="name"
                required
                ng-minlength="1" ng-maxlength="50">
        <div ng-messages="form.submitForm.name.$error"
            ng-show="form.submitForm.name.$touched">
            <div ng-message="required" class="text-danger">
                <small>@lang('site.required_validation')</small>
            </div>
            <div ng-message-exp="['minlength', 'maxlength']" class="text-danger">
                <small>@lang('site.name_length_validation')</small>
            </div>
        </div>
        <div class="text-danger">
            <small ng-bind="existedNameError"></small>
        </div>
    </div>
    <div class="form-group">
        <label class="form-control-label" for="description">@lang('site.description')</label>
        <input type="text" id="description" name="description"
                class="form-control form-control-success form-control-danger"
                ng-model="breed.description"
                placeholder="@lang('site.breed_description_placeholder')"
                ng-maxlength="200">
        <div ng-messages="form.submitForm.description.$error"
            ng-show="form.submitForm.description.$touched">
            <div ng-message-exp="['maxlength']" class="text-danger">
                <small>@lang('site.description_length_validation')</small>
            </div>
        </div>
    </div>
    <div class="form-group"
        ng-class="{
                'has-danger': form.submitForm.type.$invalid && form.submitForm.type.$touched,
                'has-success': form.submitForm.type.$valid
        }">
        <label class="form-control-label" for="type">@lang('site.type')</label>
        <select class="form-control form-control-success form-control-danger" id="type" name="type"
                ng-options="type.id as type.name for type in types track by type.id"
                ng-model="breed.type_id"
                required>
            <option value="">@lang('site.choose_type')</option>
        </select>
        <div ng-messages="form.submitForm.type.$error" ng-show="form.submitForm.type.$touched">
            <div ng-message="required" class="text-danger">
                <small>@lang('site.option_validation')</small>
            </div>
        </div>
    </div>
    <div class="form-group photo-lanscape">
        <label>@lang('site.image')</label>
        <file-input model="breed.photo" model-name="breed"></file-input>
    </div>
    <div class="form-group features" ng-if="!isUpdate">
        {{-- <button class="btn btn-secondary" ng-click="reset()">@lang('site.reset')</button> --}}
        <button class="btn btn-primary ml-2" ng-disabled="form.submitForm.$invalid" ng-click="create(breed)">
            @lang('site.create')
        </button>
    </div>
    <div class="form-group features" ng-if="isUpdate">
        <button class="btn btn-secondary" ng-click="cancel()">@lang('site.cancel')</button>
        <button class="btn btn-primary ml-2" ng-disabled="form.submitForm.$invalid" ng-click="update(breed)">
            @lang('site.update')
        </button>
    </div>
</ng-form>
