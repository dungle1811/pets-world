<div class="model" id="main">
    <div class="row">
        <div class="create-area col-12 col-sm-5 col-md-5 col-lg-5 col-xl-5">
            <div class="row">
                <div class="create-area-content col-9 col-sm-11 col-md-10 col-lg-8 col-xl-8
                            justify-content-center align-items-center
                            mx-auto my-auto">
                    @include('admin.partials.pets.breeds.create')
                </div>
            </div>
        </div>
        <div class="list-area col-12 col-sm-7 col-md-7 col-lg-7 col-xl-7">
            <div class="row">
                <div class="list-area-content col-10 col-sm-10 col-md-10 col-lg-8 col-xl-10
                            justify-content-center align-items-center
                            mx-auto my-auto">
                    @include('admin.partials.pets.breeds.filter')
                    @include('admin.partials.pets.breeds.delete-breeds')
                    @include('admin.partials.pets.breeds.list')
                    @include('admin.partials.pets.breeds.delete')
                </div>
            </div>
        </div>
    </div>
</div>
