<div class="layout model" id="main">
    <div class="frame m-auto">
        <div class="list-header row">
            <div class="search-box col-12 col-sm-5 form-group px-0">
                @include('admin.components.form-group.filterText')
            </div>
            <div class="tabs col-12 col-sm-6">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link" ng-click="resetFilterByType()"
                            ng-class="resetingActive ? 'active' : ''">
                            @lang('site.all')
                        </a>
                    </li>
                    <li class="nav-item" ng-repeat="type in types">
                        <a class="nav-link" ng-bind="type.name" ng-click="filterByType(type)"
                            ng-class="type.active ? 'active' : ''">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="content">
            <div class="card-group">
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 col-xl-3"
                    dir-paginate="pet in pets | orderBy: '-id' | filter: typeOption | filter: searchText | itemsPerPage: pageSize"
                    current-page="currentPage" pagination-id="petsPagination">
                    <div class="card my-3">
                        <div class="d-flex card-footer no-background no-border p-2">
                            <span class="col no-spacing button ml-auto">
                                <div class="btn-group">
                                    <button class="btn btn-secondary mr-2"
                                            data-toggle="modal"
                                            data-target="#detailModal-[[pet.id]]">
                                        <span class="fa fa-eye"></span>
                                    </button>
                                    <button class="btn btn-primary mr-2"
                                            ng-click="setUpdate(pet)">
                                        <span class="fa fa-pencil"></span>
                                    </button>
                                    <button class="btn btn-danger"
                                            ng-click="setDelete(pet)"
                                            data-toggle="modal" data-target="#deleteModal">
                                        <span class="fa fa-trash"></span>
                                    </button>
                                </div>
                            </span>
                        </div>
                        <img class="card-img-top"
                             ng-src="[[pet.photo_square != null ? '/upload/images/large/' + pet.photo_square : '/image/default_assets/presentation/default_pet.jpg']]">
                        <div class="card-block px-2 py-2 font">
                            <div class="d-flex">
                                <h4 class="card-title mx-auto">
                                     <span ng-bind="pet.name"></span>
                                     <span ng-class="pet.isMale ? 'fa-mars male' : 'fa-mercury female'" class="fa"></span>
                                </h4>
                            </div>
                        </div>
                        @include('admin.partials.pets.preview')
                    </div>
                </div>
            </div>
        </div>
        <confirm-delete model="deletedPet"></confirm-delete>
        <div class="pagination-container">
            <div class="row">
                <div class="col-12 col-md-4 d-flex my-2">
                    <label class="d-flex align-items-center">
                        <span>@lang('site.number_of_pets')</span>
                    </label>
                    <div class="d-flex align-items-center w-auto ml-2">
                        <select class="form-control"
                                ng-options="quantity as quantity for quantity in quantities"
                                ng-model="viewBy"
                                ng-change="setItemsPerPage()">
                        </select>
                    </div>
                </div>
                <div class="col-12 col-md-8 mt-3">
                    <div
                        dir-pagination-controls
                        boudary-link="true"
                        pagination-id="petsPagination"
                        template-url="/views/load?path=directives.dir-pagination">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
