<div class="modal fade" id="detailModal-[[pet.id]]" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="d-flex">
                    <h4 class="card-title mx-auto my-auto">
                        <span ng-bind="pet.name"></span>
                        <span ng-class="pet.isMale ? 'fa-mars male' : 'fa-mercury female'" class="fa"></span>
                    </h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pt-2 pb-0 px-2">
                <div class="container-fluid py-0">
                    <div class="card-detail">
                        <div class="photo-area">
                            <img class="photo_landscape"
                                ng-src="[[pet.photo_landscape != null ? '/upload/images/medium/' + pet.photo_landscape : '/image/default_assets/presentation/default_pet.jpg']]">
                        </div>
                        <div class="card-block px-2 py-2 font">
                            <div class="d-flex flex-column font-14">
                                <span class="font-bold">
                                    @lang('site.type'):
                                    <span ng-bind="pet.pet_type.name"></span>
                                    (<span ng-bind="pet.pet_breed.name"></span>)
                                </span>
                                <span class="font-bold">
                                    @lang('site.height'): <span ng-bind="pet.height"></span>
                                </span>
                                <span class="font-bold">
                                    @lang('site.weight'): <span ng-bind="pet.weight"></span>
                                </span>
                                <span class="font-bold status-tags-container">
                                    @lang('site.status'):
                                    <span ng-repeat="statusChild in pet.pet_status">
                                        <span class="status-tag" ng-bind="statusChild.name"></span>
                                    </span>
                                </span>
                                <span class="font-bold">
                                    @lang('site.description'): <span ng-bind="pet.description"></span>
                                </span>
                                <span class="my-auto mr-auto font-bold">
                                    @lang('site.price'): <span ng-bind="pet.price"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('site.close')</button>
            </div>
        </div>
    </div>
</div>
