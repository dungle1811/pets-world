<h5 class="title">@lang('site.types_management_title')</h5>
<p class="layout-description">
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur commodi eum quae, ipsum modi odio nulla, aliquam blanditiis dolorem.
</p>
<ng-form name="form.submitForm">
    <div ng-class="{
            'has-danger': form.submitForm.name.$invalid && form.submitForm.name.$touched || isExistedName,
            'has-success': form.submitForm.name.$valid,

        }"
        class="form-group">
        <label class="form-control-label" for="name">@lang('site.name')</label>
        <input type="text" id="name" class="form-control form-control-success form-control-danger"
                ng-model="type.name"
                ng-change="resetNameError()"
                placeholder="@lang('site.type_name_placeholder')"
                name="name"
                required
                ng-minlength="1" ng-maxlength="50">
        <div ng-messages="form.submitForm.name.$error"
            ng-show="form.submitForm.name.$touched">
            <div ng-message="required" class="text-danger">
                <small>@lang('site.required_validation')</small>
            </div>
            <div ng-message-exp="['minlength', 'maxlength']" class="text-danger">
                <small>@lang('site.name_length_validation')</small>
            </div>
        </div>
        <div class="text-danger">
            <small ng-bind="existedNameError"></small>
        </div>
    </div>
    <div class="form-group">
        <label class="form-control-label" for="description">@lang('site.description')</label>
        <input type="text" id="description" name="description"
                class="form-control form-control-success form-control-danger"
                ng-model="type.description"
                placeholder="@lang('site.type_description_placeholder')"
                ng-maxlength="200">
        <div ng-messages="form.submitForm.description.$error"
            ng-show="form.submitForm.description.$touched">
            <div ng-message-exp="['maxlength']" class="text-danger">
                <small>@lang('site.description_length_validation')</small>
            </div>
        </div>
    </div>
    <div class="form-group" >
        <label class="form-control-label" for="description">Specification</label>
        <div class="row mb-2 specification-row" ng-repeat="spec in specifications">
            <div class="col-5 pl-0 pr-1" ng-class="{
                'has-danger': form.submitForm['spec-'+$index].$invalid && form.submitForm['spec-'+$index].$touched,
                'has-success': form.submitForm['spec-'+$index].$valid,
            }">
                <input type="text"
                    ng-model="spec.name"
                    class="form-control form-control-success form-control-danger"
                    required name="spec-[[$index]]">
            </div>
            <div class="col-3 px-0">
                <select name="type_id" id="" class="form-control pl-1"
                    ng-options="type as type.name for type in unit_types track by type.id"
                    ng-model="spec.type" ng-change="spec.unit = spec.type.units[0]">
                </select>
            </div>
            <div class="col-3 pl-1 pr-0">
                <select name="type_id" id="" class="form-control pl-1"
                    ng-options="unit as unit.name for unit in spec.type.units track by unit.id"
                    ng-model="spec.unit">
                </select>
            </div>
            <div class="col-1 pl-1 pr-0 d-flex">
                <i class="fa fa-times remove"
                    ng-click="removeSpec($index)"></i>
            </div>
            <div class="col-12 p-0">
                 <div ng-messages="form.submitForm['spec-'+$index].$error"
                    ng-show="form.submitForm['spec-'+$index].$touched">
                    <div ng-message="required" class="text-danger">
                        <small>@lang('site.required_validation')</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <button class="btn btn-secondary btn-block btn-sm" ng-click="addSpec($index)"><i class="fa fa-plus"></i></button>
        </div>

    </div>
    <div class="form-group features" ng-if="!isUpdate">
        <button class="btn btn-secondary" ng-click="reset()">@lang('site.reset')</button>
        <button class="btn btn-primary ml-2" ng-disabled="form.submitForm.$invalid" ng-click="create(type)">
            @lang('site.create')
        </button>
    </div>
    <div class="form-group features" ng-if="isUpdate">
        <button class="btn btn-secondary" ng-click="cancel()">@lang('site.cancel')</button>
        <button class="btn btn-primary ml-2" ng-disabled="form.submitForm.$invalid" ng-click="update(type)">
            @lang('site.update')
        </button>
    </div>
</ng-form>
