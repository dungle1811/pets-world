<div class="list">
    <div class="list-group">
        <div class="list-group-item media"
                ng-repeat="type in types | orderBy: '-id' | filter: searchText">
            <div class="check-form d-flex mr-1">
                <input type="checkbox"
                        id="type-[[type.id]]"
                        class="check-form-input check-model"
                        ng-model="type.selected"
                        ng-change="changeCheckedOption()"
                        value="[[type.id]]"
                        name="selectedTypes[]">
                <label for="type-[[type.id]]">
                    <img ng-src="/image/default_assets/presentation/default_pet.jpg">
                    <span class="fa fa-check"></span>
                </label>
            </div>
            <div class="media-body">
                <div class="row">
                    <div class="info col-12 text-truncate">
                        <p ng-bind="type.name" class="name"></p>
                        <p ng-bind="type.description" class="description"></p>
                    </div>
                    <div class="features-area">
                        <div class="features">
                            <label ng-click="setUpdate(type)"><i class="fa fa-pencil" aria-hidden="true"></i></label>
                            <label ng-click="setDelete(type)" data-toggle="modal" data-target="#deleteModal">
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
