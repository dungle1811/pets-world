<div class="model" id="main">
    <div class="row">
        <div class="create-area col-12 col-sm-5 col-md-5 col-lg-5 col-xl-5">
            <div class="row">
                <div class="create-area-content col-12 col-sm-11 col-lg-10 col-xl-9
                            justify-content-center align-items-center
                            mx-auto my-auto">
                    @include('admin.partials.pets.types.create')
                </div>
            </div>
        </div>
        <div class="list-area col-12 col-sm-7 col-md-7 col-lg-7 col-xl-7">
            <div class="row">
                <div class="list-area-content col-12 col-sm-10 col-md-10 col-lg-8
                            justify-content-center align-items-center
                            mx-auto my-auto">
                    @include('admin.components.form-group.filterText')
                    @include('admin.partials.pets.types.delete-types')
                    @include('admin.partials.pets.types.list')
                    @include('admin.partials.pets.types.delete')
                </div>
            </div>
        </div>
    </div>
</div>
