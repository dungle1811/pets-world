<div class="list">
    <ul class="list-group">
        <div class="list-group-item media"
                ng-repeat="statusChild in status | orderBy: '-id' | filter: searchText">
            <div class="check-form d-flex mr-1">
                <input type="checkbox"
                        id="statusChild-[[statusChild.id]]"
                        class="check-form-input check-model"
                        ng-model="statusChild.selected"
                        ng-change="changeCheckedOption()"
                        value="[[statusChild.id]]"
                        name="selectedBreeds[]">
                <label for="statusChild-[[statusChild.id]]">
                    <img ng-src="/image/default_assets/presentation/default_healthy.png">
                    <span class="fa fa-check"></span>
                </label>
            </div>
            <div class="media-body">
                <div class="row">
                    <div class="info col-12 text-truncate">
                        <p ng-bind="statusChild.name" class="name"></p>
                        <p ng-bind="statusChild.description" class="description"></p>
                    </div>
                    <div class="features-area">
                        <div class="features">
                            <label ng-click="setUpdate(statusChild)"><i class="fa fa-pencil" aria-hidden="true"></i></label>
                            <label ng-click="setDelete(statusChild)" data-toggle="modal" data-target="#deleteModal">
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </ul>
</div>
@include('admin.partials.pets.status.delete')
