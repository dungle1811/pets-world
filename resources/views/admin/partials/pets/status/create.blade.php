<h5 class="title">@lang('site.status_management_title')</h5>
<p class="layout-description">
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur commodi eum quae, ipsum modi odio nulla, aliquam blanditiis dolorem.
</p>
<ng-form name="form.submitForm">
    <div ng-class="{
            'has-danger': form.submitForm.name.$invalid && form.submitForm.name.$touched || isExistedName,
            'has-success': form.submitForm.name.$valid,
        }"
        class="form-group">
        <label class="form-control-label" for="name">@lang('site.name')</label>
        <input type="text" id="name" class="form-control form-control-success form-control-danger"
                ng-model="statusChild.name"
                ng-change="resetNameError()"
                placeholder="@lang('site.status_name_placeholder')"
                name="name"
                required
                ng-minlength="1" ng-maxlength="50">
        <div ng-messages="form.submitForm.name.$error"
            ng-show="form.submitForm.name.$touched">
            <div ng-message="required" class="text-danger">
                <small>@lang('site.required_validation')</small>
            </div>
            <div ng-message-exp="['minlength', 'maxlength']" class="text-danger">
                <small>@lang('site.name_length_validation')</small>
            </div>
        </div>
        <div class="text-danger">
            <small ng-bind="existedNameError"></small>
        </div>
    </div>
    <div class="form-group">
        <label class="form-control-label" for="description">@lang('site.description')</label>
        <input type="text" id="description" name="description"
                class="form-control form-control-success form-control-danger"
                ng-model="statusChild.description"
                placeholder="@lang('site.status_description_placeholder')"
                ng-maxlength="200">
        <div ng-messages="form.submitForm.description.$error"
            ng-show="form.submitForm.description.$touched">
            <div ng-message-exp="['maxlength']" class="text-danger">
                <small>@lang('site.description_length_validation')</small>
            </div>
        </div>
    </div>
    <div class="form-group features" ng-if="!isUpdate">
        <button class="btn btn-secondary" ng-click="reset()">@lang('site.reset')</button>
        <button class="btn btn-primary ml-2" ng-disabled="form.submitForm.$invalid" ng-click="create(statusChild)">
            @lang('site.create')
        </button>
    </div>
    <div class="form-group features" ng-if="isUpdate">
        <button class="btn btn-secondary" ng-click="cancel()">@lang('site.cancel')</button>
        <button class="btn btn-primary ml-2" ng-disabled="form.submitForm.$invalid" ng-click="update(statusChild)">
            @lang('site.update')
        </button>
    </div>
</ng-form>
