<div class="pet-create model" >
    <div class="col-12 col-sm-8 col-md-7 col-lg-7 col-xl-6 create-body">
        <div class="create-list col-12 col-sm-10 col-xl-9">
            <div class="create-title">
                <h3 class="title" ng-if="!isUpdate">@lang('site.pet_create')</h3>
                <h3 class="title" ng-if="isUpdate">@lang('site.pet_update')</h3>
            </div>
            <p class="layout-description">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur commodi eum quae, ipsum modi odio nulla, aliquam blanditiis dolorem.
            </p>
            <ng-form name="form.submitForm">
                <legend>
                    @lang('site.general')
                </legend>
                <div ng-class="{
                        'has-danger': form.submitForm.name.$invalid && form.submitForm.name.$touched || isExistedName,
                        'has-success': form.submitForm.name.$valid,
                    }"
                    class="form-group">
                    <label for="name">@lang('site.name')</label>
                    <input class="form-control form-control-success form-control-danger font-format"
                        id="name"
                        name="name"
                        placeholder="Pet name"
                        ng-model="pet.name"
                        ng-change="resetNameError()"
                        required
                        ng-minlength="1" ng-maxlength="50">
                    <div ng-messages="form.submitForm.name.$error"
                        ng-show="form.submitForm.name.$touched">
                        <div ng-message="required" class="text-danger">
                            <small>@lang('site.required_validation')</small>
                        </div>
                        <div ng-message-exp="['minlength', 'maxlength']" class="text-danger">
                            <small>@lang('site.name_length_validation')</small>
                        </div>
                    </div>
                    <div class="text-danger">
                        <small ng-bind="existedNameError"></small>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">@lang('site.description')</label>
                    <input type="Text" class="form-control font-format"
                            id="name" name="description"
                            placeholder="Description"
                            ng-model="pet.description"
                            ng-maxlength="200">
                    <div ng-messages="form.submitForm.description.$error"
                        ng-show="form.submitForm.description.$touched">
                        <div ng-message-exp="['maxlength']" class="text-danger">
                            <small>@lang('site.description_length_validation')</small>
                        </div>
                    </div>
                </div>
                <legend>
                    @lang('site.photo')
                </legend>
                <div class="photo">
                    <div ng-if="isChangePhoto" class="mb-3">
                        <button class="btn btn-sm btn-primary btn-block" ng-click="showPhotoCropper()">
                            <strong>@lang('site.change_photo')</strong>
                        </button>
                    </div>
                    <div ng-if="isShowPhotoCropper"
                        class="col-12 no-spacing photo-square"
                        ng-class="isUpdate ? 'photo-container-transition' : ''">
                        <div class="form-group cover">
                            <label>@lang('site.landscape_photo')</label>
                            <div file-cropper="pet.photo_landscape"
                                output-file='pet.photo_square'>
                            </div>
                        </div>
                    </div>
                </div>
                <legend>
                    @lang('site.information')
                </legend>
                <div class="form-group">
                    <label class="form-control-label" for="rad-group">@lang('site.gender')</label>
                    <div class="radio-group" id="rad-group">
                        <div class="form-check" ng-repeat="gender in genders">
                            <label class="form-check-label" for="gender-[[gender.id]]">
                                <input type="radio" class="form-check-input" name="rad"
                                        id="gender-[[gender.id]]" value=""
                                        ng-model="pet.sex"
                                        ng-value="[[gender.bool]]"
                                        checked>
                                <span ng-bind="gender.name"><span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="type">@lang('site.type')</label>
                    <div
                        class="select-box"
                        isteven-multi-select
                        selection-mode="single"
                        input-model="inputTypes"
                        output-model="selectedType"
                        output-properties="id"
                        button-label="name"
                        item-label="name"
                        tick-property="selected"
                        max-height="150px"
                        translation="langForOne"
                        on-item-click="clickTypeFunc()"
                        on-reset="resetTypeFunc()">
                    </div>
                </div>
                <div class="form-group" ng-show="isShowBreeds">
                    <label for="breed">@lang('site.breed')</label>
                    <div
                        class="select-box breeds-box"
                        isteven-multi-select
                        selection-mode="single"
                        input-model="inputBreeds"
                        output-model="selectedBreed"
                        output-properties="id"
                        button-label="name"
                        item-label="name"
                        tick-property="selected"
                        max-height="150px"
                        translation="langForOne">
                    </div>
                </div>
                <div ng-show="isShowUnitProperties">
                    <div ng-class="{
                            'has-danger': form.submitForm.height.$invalid && form.submitForm.height.$touched,
                            'has-success': form.submitForm.height.$valid,
                        }"
                        class="form-group">
                        <label for="height">@lang('site.height')</label>
                        <input type="Number" min="0"
                                class="form-control form-control-success form-control-danger font-format"
                                placeholder="0.0" ng-model="pet.height">
                    </div>
                    <div ng-class="{
                            'has-danger': form.submitForm.weight.$invalid && form.submitForm.weight.$touched,
                            'has-success': form.submitForm.weight.$valid,
                        }"
                        class="form-group">
                        <label for="weight">@lang('site.weight')</label>
                        <input type="Number" min="0"
                                class="form-control form-control-success form-control-danger font-format"
                                placeholder="0.0" ng-model="pet.weight">
                    </div>
                </div>
                <div class="form-group">
                    <label for="status">@lang('site.status')</label>
                    <div
                        class="select-box"
                        isteven-multi-select
                        input-model="inputStatus"
                        output-model="selectedStatus"
                        output-properties="id"
                        button-label="name"
                        item-label="name"
                        tick-property="selected"
                        max-height="150px"
                        translation="langForMore">
                    </div>
                </div>
                <div ng-class="{
                        'has-danger': form.submitForm.price.$invalid && form.submitForm.price.$touched,
                        'has-success': form.submitForm.price.$valid,
                    }"
                    class="form-group">
                    <label for="price">@lang('site.price')</label>
                    <input type="Number" min="0"
                            class="form-control form-control-success form-control-danger font-format"
                            name="price" id="price"
                            required
                            placeholder="0.0" ng-model="pet.price">
                    <div ng-messages="form.submitForm.price.$error"
                        ng-show="form.submitForm.price.$touched">
                        <div ng-message="required" class="text-danger">
                            <small>@lang('site.required_validation')</small>
                        </div>
                    </div>
                </div>
                <div class="create-btn pt-2" ng-if="!isUpdate">
                    <div class="form-group ml-auto">
                        {{-- <button class="btn btn-secondary mr-1">Reset</button> --}}
                        <button class="btn btn-primary ml-1"
                                ng-disabled="form.submitForm.$invalid"
                                ng-click="create(pet)">@lang('site.create')</button>
                    </div>
                </div>
                <div class="form-group features d-flex justify-content-end" ng-if="isUpdate">
                    <button class="btn btn-secondary mr-1" ng-click="cancel()">@lang('site.cancel')</button>
                    <button class="btn btn-primary ml-2" ng-disabled="form.submitForm.$invalid" ng-click="update(pet)">
                        @lang('site.update')
                    </button>
                </div>
            </ng-form>
        </div>
    </div>
</div>
