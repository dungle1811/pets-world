<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" ng-app="PetWorldModule" ng-controller="MainController">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@lang('site.project_name')</title>


        <link rel="stylesheet" href="bundled/css/admin.css">
    </head>
    <body>
        <input type="checkbox" name="menu" id="menu" class="d-none menu">
        <!-- Main Container -->
        <div class="container-fluid">
            <!-- Page Header -->
            <div class="box header">
                @include('admin.partials.components.header')
            </div>
            <!-- Page Content -->
            <label class="fade-body" for="menu"></label>
            <div class="box page-container">
                <!-- Sidebar Content -->
                <div class="page-sidebar">
                    @include('admin.partials.components.sidebar')
                </div>
                <!-- Container -->
                <div class="main-wrapper">
                    @include('admin.partials.components.container')
                </div>
            </div>
        </div>

        <!-- javascript -->
        <script type="text/javascript">
            (function() {
                window.Auth = {
                    api_token : "{{ Auth::user()->api_token }}"
                }
            }())
        </script>
        <script src="bundled/js/admin.js"></script>
    </body>
</html>
