<input type="checkbox" value="" class="check-all-input" id="check-all"
        ng-model="isSelectedAll"
        ng-click="checkAll()">
<label for="check-all" class="check-all-title col-5">@lang('site.select_all')</label>
<div class="options col-7 d-flex justify-content-end" ng-hide="length == 0">
    <label class="uncheck-option"
            ng-if="length >= 2"
            ng-click="uncheckAll()">@lang('site.unselect_all')</label>
    <div class="delete-option">
        <i class="fa fa-times" data-toggle="modal" data-target="#deleteMultipleModal"></i>
    </div>
</div>
