<div class="form-group filter-area d-flex">
    <input type="text" class="form-control search-input" id="search-input"
                placeholder="@lang('site.search_placeholder')" ng-model="searchText">
    <button class="btn btn-secondary btn-sm ml-1 reset-filter" ng-click="resetFilter()">@lang('site.reset')</button>
</div>
