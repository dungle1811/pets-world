<div class="popover" role="tooltip">
    <div class="popover-arrow"></div>
    <div class="popover-content">
        <div class="features">
            <label ng-click="setUpdate(type)"><i class="fa fa-pencil" aria-hidden="true"></i></label>
            <label ng-click="setDelete(type)" data-toggle="modal" data-target="#deleteModal">
                <i class="fa fa-times" aria-hidden="true"></i>
            </label>
        </div>
    </div>
</div>
