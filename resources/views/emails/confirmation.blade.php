We are very glad when you visit our website.<br>
Thanks for creating an account.<br>
Please follow the link below to verify your email address :<br>
{{route('view.emails.confirmation', $confirmation_token)}}<br>
Thank you,<br>
{{ trans('site.project_name') }}
