@extends('homepage.master')
@section('master.body')
<div class="body">
    <div class="container">
        <div class="link">
            <ol class="breadcrumb bg-white">
                <li class="breadcrumb-item"><a href="{{ route('view.home.index') }}">Homepage</a></li>
                <li class="breadcrumb-item"><a href="{{ route('view.pets') }}">Pets</a></li>
                <li class="breadcrumb-item active">{{ $pet->name }}</li>
            </ol>
        </div>
        <div class="row d-flex">
            <div class="img col-md-5">
                <div class="img-card col-md-12">
                    <img class="img-responsive"
                        src="{{$pet->photo_landscape != null ? '/upload/images/medium/'.$pet->photo_landscape : '/image/default_assets/presentation/default_pet.jpg'}}">
                </div>
                <div class="card-group col-md-12">
                    <div id="img-control" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            @for($i=0 ; $i<3 ; $i++)
                                <div class="carousel-item {{ $i==0 ? 'active' : ''}}">
                                    @for($j=0 ; $j<4 ; $j++)
                                        <div class="col-12 col-sm-6 col-md-3 img-mini">
                                            <div class="card-item card">
                                                <div class="avatar">
                                                    <img class="card-img-top"
                                                        src="{{$pet->photo_landscape != null ? '/upload/images/small/'.$pet->photo_landscape : '/image/default_assets/presentation/default_pet.jpg'}}">
                                                </div>
                                            </div>
                                        </div>
                                    @endfor
                                </div>
                            @endfor
                        </div>
                    </div>
                    <a class="carousel-control-prev" data-target="#img-control" role="button" data-slide="prev">
                       <i class="fa fa-chevron-left" aria-hidden="true"></i>
                       <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" data-target="#img-control" role="button" data-slide="next">
                      <i class="fa fa-chevron-right" aria-hidden="true"></i>
                       <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="detail col-md-7">
                <div class="title">
                    <h1>{{ $pet->name }}</h1>
                </div>
                <div class="describe">
                    <h5>Describe: </h5>
                    <p>{{ $pet->description}}</p>
                </div>
                <hr>
                <div class="price">
                    <h3>Price: 80.000 VND</h3>
                    <div class="sale-off">
                        <h6>Before Sale-off: {{ $pet->price }} VND</h6>
                        <h6>Save: 20%</h6>
                    </div>
                </div>
                <div class="shop d-flex">
                    <h6>Shop:</h6>
                    <a href="{{ route('view.home.index') }}">Pet's House</a>
                </div>
                <hr>
            </div>
        </div>
        <div class="row2 d-flex">
            <div class=" img col-md-9">
                <img src="{{$pet->photo_landscape != null ? '/upload/images/large/'.$pet->photo_landscape : '/image/default_assets/presentation/default_pet.jpg'}}">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <img src="{{$pet->photo_landscape != null ? '/upload/images/large/'.$pet->photo_landscape : '/image/default_assets/presentation/default_pet.jpg'}}">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
            </div>
            <div class="card related-accessories col-md-3">
                <h4>Related Accessories</h4>
                <div class="item" ng-repeat="i in [1,2,3]">
                    <div class="card-item card">
                        <div class="img">
                            <img class="card-img-top" src="/image/default_assets/new-product-1.jpg" alt="Card image cap">
                        </div>
                        <div class="card-block">
                            <div class="describe">
                                <p6>Name-Size-Color-Origin</p6>
                            </div>
                            <div class="price">
                                <h5>480.000đ VND</h5>
                                <div class="sale-off">
                                    <p3><del>600.000 VND</del> -20%</p3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="comment row d-flex">
            <div class="form-group col-9">
                <label for="cmt"><h5>Comments</h5></label>
                <textarea class="form-control" id="cmt" rows="3" placeholder="Enter your comments ..."></textarea>
                <div class="btn">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="button" class="btn">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
