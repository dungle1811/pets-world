@extends('homepage.master')
@section('master.body')
<div class="pet-ad">
    <div id="pet-ad-control" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
                <img class="d-block img-fluid" src="/image/default_assets/ad-1.jpg" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block img-fluid" src="/image/default_assets/ad-2.jpg" alt="Second slide">
            </div>
            <div class="carousel-item">
                <img class="d-block img-fluid" src="/image/default_assets/ad-3.jpg" alt="Third slide">
            </div>
        </div>
        <a class="carousel-control-prev" data-target="#pet-ad-control" role="button" data-slide="prev">
           <span class="carousel-control-prev-icon" aria-hidden="true"></span>
           <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" data-target="#pet-ad-control" role="button" data-slide="next">
           <span class="carousel-control-next-icon" aria-hidden="true"></span>
           <span class="sr-only">Next</span>
        </a>
        <ol class="carousel-indicators">
            <li data-target="#pet-ad-control" data-slide-to="0" class="active"></li>
            <li data-target="#pet-ad-control" data-slide-to="1"></li>
            <li data-target="#pet-ad-control" data-slide-to="2"></li>
        </ol>
    </div>
</div>
<div class="pet-body container mb-5">
    <div class="row">
        @php
            $pet_types = App\Model\PetType::all();
        @endphp
        <div class="pet-sidebar col-12 col-md-3">
            <div class="sort">
                <div class="pet">
                    <button class="pet-btn" type="button" data-toggle="collapse" data-target="#collapsePet" aria-expanded="false" aria-controls="collapsePet"
                    ng-click="getAllPets()">
                        <b>Pets</b>
                    </button>
                    <div class="collapse" id="collapsePet">
                        <div class="card">
                            <div class="form">
                                <button class="type-btn" ng-click="resetPetsFilter()" data-target="#collapse-0">
                                    <b>All</b>
                                </button>
                            </div>
                            @foreach($pet_types as $pet_type)
                                <div class="form">
                                    <button class="type-btn" data-toggle="collapse" data-target="#collapse-{{$pet_type->id}}" aria-expanded="false" aria-controls="collapse-{{$pet_type->id}}"
                                        ng-click="getBreedsByType({{$pet_type->id}})">
                                        <b>{{$pet_type->name}}</b>
                                    </button>
                                    <div class="collapse pet-types-collapse" id="collapse-{{$pet_type->id}}">
                                        <div class="card" ng-repeat="breed in breedsByType">
                                            <div class="form-check"
                                                ng-class="breed.isActive ? 'active' : ''"
                                                ng-click="getBreedName(breed)">
                                                <label class="form-check-label" ng-bind="breed.name">
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>    
                            @endforeach
                        </div>
                    </div>
                    <hr>
                </div>
                <div class="price">
                    <button class="price-btn" type="button" data-toggle="collapse" data-target="#collapsePrice" aria-expanded="false" aria-controls="collapsePrice">
                        <b>Price</b>
                    </button>
                    <div class="collapse" id="collapsePrice">
                        <div class="card">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="option1"> <200.000đ
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="option2"> 200.000đ - 500.000đ
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="option3"> 500.000đ - 1.000.000đ
                                </label>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
            </div>
        </div>
        <div class="main-item col-sm-12 col-md-9">
            <div class="page-number-and-sort row justify-content-between">
                <select class="custom-select mb-2  mb-sm-0" id="inlineFormCustomSelect">
                    <option selected>Sort by...</option>
                    <option value="1">Price: high to low</option>
                    <option value="2">Price: low to high</option>
                    <option value="3">Best seller</option>
                    <option value="3">Top rates</option>
                    <option value="3">Species</option>
                </select>
                <div
                    dir-pagination-controls
                    boudary-link="true"
                    pagination-id="petsPagination"
                    template-url="/views/load?path=directives.dir-pagination">
                </div>
            </div>
            <div class="item">
                <div class="card-group col-md-12">
                    <div class="col-12 col-sm-6 col-md-3 item-detail"
                        dir-paginate="pet in pets | orderBy:'-id' | filter: breedNameFilter | filter: typeNameFilter | itemsPerPage: pageSize"
                        current-page="currentPage" pagination-id="petsPagination">
                        <div class="card-item card" ng-click="goToPetDetail(pet.id)">
                            <div class="avatar">
                                <img class="card-img-top" 
                                    ng-src="[[pet.photo_landscape != null ? '/upload/images/medium/' + pet.photo_landscape : '/image/default_assets/presentation/default_pet.jpg']]">
                            </div>
                            <div class="card-block">
                                <div class="describe">
                                    <p6 ng-bind="pet.name"></p6>
                                </div>
                                <div class="price">
                                    <h5>80.000 VND</h5>
                                    <div class="sale-off">
                                        <p3><del ng-bind="pet.price"> VND</del> -20%</p3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="no-data"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="show-more-btn hidden-sm-up row justify-content-center">
        <button type="button" name="show-more" >Show more</button>
    </div>
</div>
@endsection
