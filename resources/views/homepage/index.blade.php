@extends('homepage.master')
@section('master.body')
<div class="main-background">
    <div class="slogan">
        <h1>The Dog</h1>
        <h3>is the only thing on earth <br>that will love you more than <br>you love yourself.</h3>
    </div>
</div>
<div class="body">
    <div class="list container">
        <div class="tab best-seller">
            <div class="title">
                <p3>Top sellers of the month</p3>
            </div>
            @php
                $pets = App\Model\Pet::all();
                $carousel_items = array();
                $card_items = array();
                $count = 0;
                for($i=0; $i<count($pets); $i++) {
                    array_push($card_items, $pets[$i]);
                    $count++;
                    if ($count == 4 || ($i == (count($pets) - 1) && $count < 4)) {
                        array_push($carousel_items, $card_items);
                        unset($card_items);
                        $card_items = array();
                        $count = 0;
                    }
                }
            @endphp
            <div class="item col-12 row even">
                <div class="img promotion-ad best-seller col-md-3 ">
                    <div class="title-img row justify-content-center">
                        <h5>Top seller of the month</h5>
                    </div>
                    <div class="button buy-now row justify-content-center ">
                        <a href="http://localhost:3000/#!/pets"><button type="button" name="button">Buy Now</button></a>
                    </div>
                </div>
                <div class="card-group col-md-9">
                    <div id="hot-item-control" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            @for($i=0; $i<count($carousel_items); $i++)
                                <div class="carousel-item {{$i == 0 ? 'active' : ''}}">
                                    @for($j=0; $j<count($carousel_items[$i]); $j++)
                                        <div class="col-12 col-sm-6 col-md-3 item-detail">
                                            <div class="card-item card" ng-click="goToPetDetail({{ $carousel_items[$i][$j]->id }})">
                                                <div class="avatar">
                                                    <img class="card-img-top" 
                                                    src="{{$carousel_items[$i][$j]->photo_landscape ? '/upload/images/medium/'.$carousel_items[$i][$j]->photo_landscape : '/image/default_assets/presentation/default_pet.jpg'}}"
                                                    ui-sref="pet_detail"
                                                    ng-click="getPetDetail({{$carousel_items[$i][$j]}})">
                                                </div>
                                                <div class="card-block">
                                                    <div class="describe">
                                                        <p6>{{$carousel_items[$i][$j]->name}}</p6>
                                                    </div>
                                                    <div class="price">
                                                        <h5>480.000 VND</h5>
                                                        <div class="sale-off">
                                                            <p3><del>{{$carousel_items[$i][$j]->price}} VND</del> -20%</p3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endfor
                                </div>
                            @endfor
                        </div>
                    </div>
                    <a class="carousel-control-prev" data-target="#hot-item-control" role="button" data-slide="prev">
                       <i class="fa fa-chevron-left" aria-hidden="true"></i>
                       <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" data-target="#hot-item-control" role="button" data-slide="next">
                      <i class="fa fa-chevron-right" aria-hidden="true"></i>
                       <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="tab sale-off">
            <div class="title">
                <p3>Sale off - <i>Summer Promotion</i></p3>
            </div>
            <div class="item col-12 row odd">
                <div class="img promotion-ad sale-off col-md-3">
                    <div class="title-img row justify-content-center ">
                        <h5>SALE OFF-<i>Summer Promotion</i></h5>
                    </div>
                    <div class="button buy-now row justify-content-center">
                        <a href="http://localhost:3000/#!/pets"><button type="button" name="button">Buy Now</button></a>
                    </div>
                </div>
                <div class="card-group col-md-9">
                    <div id="sale-off-control" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner" role="lisstbox">
                            <div class="carousel-item active">
                                <div class="col-12 col-sm-6 col-md-3 item-detail" ng-repeat="i in [1,2,3,4]">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/dog-accessories-1.jpg" alt="Card image cap" ui-sref="pet_detail">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="col-12 col-sm-6 col-md-3 item-detail" ng-repeat="i in [1,2,3,4]">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/dog-accessories-2.jpg" alt="Card image cap" ui-sref="pet_detail">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="col-12 col-sm-6 col-md-3 item-detail" ng-repeat="i in [1,2,3,4]">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/dog-accessories-3.jpg" alt="Card image cap" ui-sref="pet_detail">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <a class="carousel-control-prev" data-target="#sale-off-control" role="button" data-slide="prev">
                       <i class="fa fa-chevron-left" aria-hidden="true"></i>
                       <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" data-target="#sale-off-control" role="button" data-slide="next">
                      <i class="fa fa-chevron-right" aria-hidden="true"></i>
                       <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="tab new-item">
            <div class="title">
                <p3>New Products - <i>Best for your pet</i></p3>
            </div>
            <div class="item pet col-12 row even">
                <div class="img promotion-ad new-item col-md-3">
                    <div class="title-img row justify-content-center">
                        <h5>NEW PRODUCT  <br><i>  Best for your pet</i></h5>
                    </div>
                    <div class="button buy-now row justify-content-center">
                        <a href="http://localhost:3000/#!/pets"><button type="button" name="button">Buy Now</button></a>
                    </div>
                </div>
                <div class="card-group col-md-9">
                    <div id="new-product-control" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner" role="lisstbox">
                            <div class="carousel-item active">
                                <div class="col-12 col-sm-6 col-md-3 item-detail" ng-repeat="i in [1,2,3,4]">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/new-product-1.jpg" alt="Card image cap" ui-sref="pet_detail">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="col-12 col-sm-6 col-md-3 item-detail" ng-repeat="i in [1,2,3,4]">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/new-product-2.jpg" alt="Card image cap" ui-sref="pet_detail">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="col-12 col-sm-6 col-md-3 item-detail" ng-repeat="i in [1,2,3,4]">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/new-product-3.jpg" alt="Card image cap" ui-sref="pet_detail">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <a class="carousel-control-prev" data-target="#new-product-control" role="button" data-slide="prev">
                       <i class="fa fa-chevron-left" aria-hidden="true"></i>
                       <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" data-target="#new-product-control" role="button" data-slide="next">
                      <i class="fa fa-chevron-right" aria-hidden="true"></i>
                       <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
