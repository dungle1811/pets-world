<!DOCTYPE html>
<html lang="en" ng-app="PetWorldModule" ng-controller="HomepageController">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@lang('site.project_name')</title>
    <link rel="stylesheet" href="/bundled/css/homepage.css">
</head>
<body>

    @include('homepage.components.navbar')

    @yield('master.body')

    @include('homepage/components/footer')

    <script src="/bundled/js/homepage.js"></script>
</body>
</html>
