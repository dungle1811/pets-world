<nav class="navbar navbar-toggleable navbar-dark fixed-top" ng-class="{'transparent-navbar' : current_state === 'index', 'black-navbar' : current_state !== 'index'}">
    <div class="row m-0 w-100">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fa fa-bars"></i>
        </button>
        <div class="col-md-1">
            <a class="navbar-brand" href="{{ route('view.home.index') }}">Home</a>
        </div>
        <div class="collapse navbar-collapse col-md-11" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto col-md-6">
                {{-- Get pet types and pet breeds --}}
                <div class="dropdown">
                    <button class="dropbtn" ng-click="goToPets()">Pets</button>
                </div>
                {{-- End get data --}}

                {{-- Dom data --}}
                @php
                    $links = ['Link 1', 'Link 2', 'Link 3'];
                @endphp
                <div class="dropdown">
                    <button class="dropbtn">Services</button>
                    <div class="dropdown-content">
                        @for($i=0; $i<count($links); $i++)
                            <a href="#">{{$links[$i]}}</a>    
                        @endfor
                    </div>
                </div>
                <div class="dropdown">
                    <button class="dropbtn">Accessories</button>
                    <div class="dropdown-content">
                        @for($i=0; $i<count($links); $i++)
                            <a href="#">{{$links[$i]}}</a>    
                        @endfor
                    </div>
                </div>
                <div class="dropdown">
                    <button class="dropbtn">Trainers</button>
                    <div class="dropdown-content">
                        @for($i=0; $i<count($links); $i++)
                            <a href="#">{{$links[$i]}}</a>    
                        @endfor
                    </div>
                </div>
                <a href="/admin"><button class="dropbtn">Admin</button></a>
                {{-- End dom data --}}
            </ul>
            <div class="search-box col-md-4">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search...">
                    <div class="input-group-btn">
                        <button class="btn btn-secondary" type="button">
                            <label for="search-icon">
                                <i class="fa fa-search" aria-hidden="true"></i>
                            </label>
                        </button>
                    </div>
                </div>
            </div>
            <form class="login form-inline my-2 my-lg-0 col-md-2 row justify-content-center">
                @if (!Auth::check())
                    <div class="login-link">
                        <a class="nav-link" href="{{route('view.auth.login')}}">Login</a>
                    </div>
                @else
                    <div class="logout-link">
                        <a class="nav-link" href="{{route('view.auth.logout')}}">Logout</a>
                    </div>
                @endif
            </form>
        </div>
    </div>
</nav>
