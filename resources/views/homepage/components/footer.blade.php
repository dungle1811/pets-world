<footer>
    <div class="main container text-center center-block d-flex">
        <div class="row text-center">
            <a class="nav-link" href="#">WHAT WE DO</a>
            <a class="nav-link" href="#">OUR WORK</a>
            <a class="nav-link" href="#">COMPANY</a>
            <a class="nav-link" href="#">CONTACT</a>
        </div>
        <div class="social">
            <a href="https://www.facebook.com/bootsnipp"><i id="social-fb" class="fa fa-facebook-square fa-3x social"></i></a>
            <a href="https://twitter.com/bootsnipp"><i id="social-tw" class="fa fa-twitter-square fa-3x social"></i></a>
            <a href="https://plus.google.com/+Bootsnipp-page"><i id="social-gp" class="fa fa-google-plus-square fa-3x social"></i></a>
            <a href="mailto:bootsnipp@gmail.com"><i id="social-em" class="fa fa-envelope-square fa-3x social"></i></a>
        </div>
        <p>Number...Street...District...City...Country...</p>
        <p>05113.12345677</p>
    </div>
</footer>
