<div class="cg-notify-message" ng-click="$close()" ng-class="[$classes, 
    $position === 'center' ? 'cg-notify-message-center' : '',
    $position === 'left' ? 'cg-notify-message-left' : '',
    $position === 'right' ? 'cg-notify-message-right' : '']"
    ng-style="{'margin-left': $centerMargin}">
	
	<div class="icon">
		<i class="fa"></i>
	</div>

    <div ng-show="!$messageTemplate" class="template">
        [[ $message ]]
    </div>

    <div ng-show="$messageTemplate" class="template cg-notify-message-template">
        
    </div>

</div>