<div class="modal fade" id="deleteMultipleModal" tabindex="-1" role="dialog"
        aria-labelledby="deleteMultipleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteMultipleModalLabel">@lang('site.delete')</h5>
                <button class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    @lang('site.delete_models_confirm_1') <strong>@lang('site.delete_models_confirm_2') [[label]]</strong> @lang('site.delete_models_confirm_3')
                    @lang('site.delete_models_confirm_4')
                </p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal">@lang('site.no_answer')</button>
                <button class="btn btn-danger deleted" ng-click="deleteMultiple()">@lang('site.yes_answer')</button>
            </div>
        </div>
    </div>
</div>
