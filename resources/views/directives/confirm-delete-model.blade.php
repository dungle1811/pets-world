<div class="modal fade" tabindex="-1" id="deleteModal" role="dialog"
        aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">@lang('site.delete')</h5>
                <button class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>@lang('site.delete_model_confirm') <strong>[[modelName]]</strong>?</p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal">@lang('site.no_answer')</button>
                <button class="btn btn-danger deleted" ng-click="delete()">@lang('site.yes_answer')</button>
            </div>
        </div>
    </div>
</div>
