<div class="crop-area">
    <input type="file" id="fileInput"
            file-model="inputFile">
    <label for="fileInput" class="btn btn-sm btn-primary btn-block">
        <i class="mr-1 fa fa-upload"></i>
        Choose
    </label>
    <div class="crop-photo photo-bg" ng-class="isModel ? 'photo-bg-change' : 'photo-bg'">
        <img-crop
            image="imageInput"
            result-image="squareImage"
            area-type="square"
            result-image-size="150">
        </img-crop>
    </div>
    <div class="cropped-photo">
        <label>Square photo:</label>
        <div ng-class="isModel ? 'photo-bg-change' : 'photo-bg'">
            <div class="square-photo">
                <img ng-src="[[squareImage]]"/>
            </div>
        </div>
    </div>
</div>
