<div class="file-uploader">
    <input type="file" id="fileInput"
            file-model="model">
    <label for="fileInput" class="btn btn-sm btn-primary btn-block">
        <i class="mr-1 fa fa-upload"></i>
        Choose
    </label>
    <div class="photo-area photo-bg">
        <img ng-if="model !== undefined" id="photoPreviewed" ng-src="[[imageSrc]]">
        <div ng-if="model === undefined" class="no-image-previewer d-flex"></div>
    </div>
</div>
