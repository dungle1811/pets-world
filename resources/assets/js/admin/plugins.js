//Import here
window.$ = window.jQuery = require('jquery');
window.Tether = require('tether');
require('bootstrap');

window.angular = require('angular');
require('angular-ui-router');
require('angular-messages');
require('angular-animate');
require('@cgross/angular-notify');
//end import
