(function() {
    'use strict';

    angular
        .module('PetWorldModule')
        .directive('confirmDeleteModels', confirmDeleteModels);

    /* @ngInject */
    function confirmDeleteModels() {
        return {
            restrict: 'E',
            scope: {
                label: '@'
            },
            templateUrl: parseView('directives.confirm-delete-models'),
            link: (scope, el, attr) => {
                scope.deleteMultiple = () => {
                    let mainScope = angular.element('#main').scope();
                    mainScope.deleteMultiple();
                    $('.deleted').attr('data-dismiss', 'modal');
                }
            }
        }
    }
})();
