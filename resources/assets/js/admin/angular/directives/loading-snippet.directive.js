(function() {
    'use strict';

    angular
        .module('PetWorldModule')
        .directive('loading', loading);

    /* @ngInject */
    function loading() {
        return {
            restrict: 'EA',
            replace:true,
            templateUrl: parseView('directives.loading-snippet'),
            link: (scope, el, attr) => {
                scope.$watch('isLoading', (val) => {
                    if (val) {
                        $(el).show();
                    }else{
                        $(el).hide();
                    }
                });
            }
        }
    }
})();
