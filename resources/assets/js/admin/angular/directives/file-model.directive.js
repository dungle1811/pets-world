(function() {
    'use strict';

    angular
        .module('PetWorldModule')
        .directive('fileModel', fileModel);

    fileModel.$inject = ['$parse'];

    /* @ngInject */
    function fileModel($parse) {
        return {
            restrict: 'A',
            link: (scope, el, attr, ctrl) => {
                let model = $parse(attr.fileModel);
                let modelSetter = model.assign;

                el.bind('change', () => {
                    scope.$apply(() => {
                        modelSetter(scope, el[0].files[0])
                    })
                })
            }
        }
    }
})();
