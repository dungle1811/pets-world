angular
    .module('PetWorldModule')
    .config(MultiSelectDirectiveConfig);

MultiSelectDirectiveConfig.$inject = ['$provide'];

function MultiSelectDirectiveConfig($provide) {
    $provide.decorator('istevenMultiSelectDirective', ($delegate) => {
        let directive = $delegate[0];

        directive.templateUrl = parseView('directives.multi-select');
        directive.controller = Controller;

        Controller.$inject = [];

        function Controller () {
            setWidth();

            $(window).resize(() => {
                setWidth();
            })
        }

        function setWidth() {
            let parentWidth = $('.multiSelect').outerWidth();
            $('.checkboxLayer').outerWidth(parentWidth);
        }

        return $delegate;
    })
}
