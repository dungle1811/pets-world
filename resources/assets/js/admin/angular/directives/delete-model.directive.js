(function() {
    'use strict';

    angular
        .module('PetWorldModule')
        .directive('confirmDelete', confirmDelete);

    /* @ngInject */
    function confirmDelete() {
        return {
            restrict: 'EA',
            scope: {
                model: '='
            },
            templateUrl: parseView('directives.confirm-delete-model'),
            link: (scope, element, attrs) => {
                scope.$watch('model', deleteModel);

                function deleteModel(model){
                    if (model) {
                        scope.modelName = model.name;
                    }
                }

                scope.delete = () => {
                    let mainScope = angular.element("#main").scope();
                    mainScope.delete(scope.model);
                    $('.deleted').attr('data-dismiss', 'modal');
                };
            },
        }
    }
})();
