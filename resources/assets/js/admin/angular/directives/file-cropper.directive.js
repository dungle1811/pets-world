(function() {
    'use strict';

    angular
        .module('PetWorldModule')
        .directive('fileCropper', fileCropper);

    /* @ngInject */
    function fileCropper($timeout) {
        return {
            restrict: 'A',
            scope: {
                inputFile: '=fileCropper',
                outputFile: '=outputFile'
            },
            templateUrl: parseView('directives.file-cropper'),
            link: (scope, el, attr) => {
                scope.squareImage = '';
                scope.fileReaderSupported = window.FileReader != null;

                scope.cancelEvent = () => {
                    scope.imageInput = '';
                    scope.squareImage = '';
                    scope.isModel = false;
                }

                /**
                 * - If image is choosed from Choose File Dialog, model will be a File object.
                 * - If image is available and called from local folder (when update),
                 *      model will be a string (image name).
                 * - The file uploader will not create a new if user does not update other.
                 * ----------------------------------------------------------------------------
                 * - Both imageInput and squareImage are data URI.
                 * - Convert file to data URI to preview image.
                 * - Convert data URI to File object to submit data (create, update).
                 */
                scope.$watch('inputFile', (inputFile) => {
                    if (inputFile) {
                        scope.isModel = true;
                        scope.imageInput = '/upload/images/medium/' + inputFile;
                    } else {
                        scope.imageInput = '/image/default_assets/presentation/default_pet.jpg';
                        scope.cancelEvent();
                    }

                    if (scope.fileReaderSupported && inputFile && inputFile.type && inputFile.type.indexOf('image') > -1) {
                        scope.readFile(inputFile);
                    }
                })

                // Convert local file path of square photo to File object when update model
                try {
                    let splittedNameArr = scope.outputFile.split('.');
                    let string = splittedNameArr[0] + 'change' + '.' + splittedNameArr[1];
                    scope.$watch('squareImage', (squareImage) => {
                        scope.outputFile = scope.dataURItoFileObject(squareImage, string);
                    })
                } catch (e) {

                } finally {

                }

                // Convert File object to data URI which will help preview image
                scope.readFile = (file) => {
                    let fileReader = new FileReader();
                    fileReader.readAsDataURL(file);
                    fileReader.onload = (e) => {
                        $timeout(() => {
                            scope.imageInput = e.target.result;

                            // Cropped image name
                            let splittedNameArr = file.name.split('.');
                            let string = splittedNameArr[0] + '_square' + '.' + splittedNameArr[1];

                            // Convert data URI to File object
                            scope.$watch('squareImage', (squareImage) => {
                                scope.outputFile = scope.dataURItoFileObject(scope.squareImage, string);
                            })
                        })
                    }
                }

                // Convert base64 file to file object
                scope.dataURItoFileObject = (dataURI, photoName) => {
                    let byteString;
                    if (dataURI.split(',')[0].indexOf('base64') >= 0) {
                        byteString = atob(dataURI.split(',')[1]);
                    } else {
                        byteString = unescape(dataURI.split(',')[1]);
                    }

                    let mimeString = '';
                    if (dataURI) {
                        mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];    
                    }


                    let ia = new Uint8Array(byteString.length);
                    for (let i = 0; i < byteString.length; i++) {
                        ia[i] = byteString.charCodeAt(i);
                    }

                    let blob = new Blob([ia], {
                        type: mimeString
                    });

                    return new File([blob], photoName);
                }
            }
        }
    }
})();
