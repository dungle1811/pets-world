(function() {
    'use strict';

    angular
        .module('PetWorldModule')
        .directive('fileInput', fileInput);

    /* @ngInject */
    fileInput.$inject = ['$timeout']

    function fileInput($timeout) {
        return {
            restrict: 'E',
            scope: {
                model: '=',
                modelName: '@'
            },
            templateUrl: parseView('directives.file-uploader'),
            link: (scope, el, attr) => {
                // Preview photo
                scope.fileReaderSupported = window.FileReader != null;

                /**
                 * - If image is choosed from Choose File Dialog, model will be a File object.
                 * - If image is available and called from local folder (when update),
                 *      model will be a string (image name).
                 * - The file uploader will not create a new if user does not update other.
                 * - Convert file to data URI to preview image.
                 * - Convert data URI to File object to submit data (create, update).
                 */
                scope.$watch('model', (model) => {
                    if (model) {
                        scope.imageSrc = '/upload/images/medium/' + model;
                    } else {
                        switch (scope.modelName) {
                            case 'breed':
                                scope.imageSrc = '/image/default_assets/presentation/default_pet.jpg';
                                break;
                            case 'user':
                                scope.imageSrc = '/image/default_assets/presentation/default_user.png';
                                break;
                            default: scope.imageSrc = '/image/default_assets/presentation/default_photo.png';
                        }
                    }

                    if (scope.fileReaderSupported && model && model.type && model.type.indexOf('image') > -1) {
                        scope.readFile(model);
                    }
                })

                // Convert File object to data URI which will help preview image
                scope.readFile = (file) => {
                    let fileReader = new FileReader();
                    fileReader.readAsDataURL(file);
                    fileReader.onload = (e) => {
                        $timeout(() => {
                            scope.imageSrc = e.target.result;
                        })
                    }
                }
            }
        }
    }
})();
