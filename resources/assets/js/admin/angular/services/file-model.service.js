(function() {
    'use strict';

    angular
        .module('PetWorldModule')
        .service('$formDataHttp', FormDataHttp);

    FormDataHttp.$inject = ['$authHttp'];

    /* @ngInject */
    function FormDataHttp($http) {
        this.post = (url, model) => {
            let fd = new FormData();
            for (let key in model) {
                fd.append(key, model[key]);
            }

            return $http.post(url, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            });
        }
    }
})();
