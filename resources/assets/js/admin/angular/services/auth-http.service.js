(function() {
    'use strict';

    angular
        .module('PetWorldModule')
        .service('$authHttp', authHttp);

    authHttp.$inject = ['$http'];

    /* @ngInject */
    function authHttp($http) {
        this.get = (url, data = {}) => {
            data.api_token = Auth.api_token;
            return $http.get(url, {
                params: data
            });
        };

        this.post = (url, data = {}, option = {}) => {
            //FormData case
            if (data instanceof FormData && option !== {}) {
                data.append('api_token', Auth.api_token);
                return $http.post(url, data, option)
            }
            //Normal case
            else {
                data.api_token = Auth.api_token;
                return $http.post(url, data)
            }
        }

        return this;
    }
})();
