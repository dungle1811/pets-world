(function() {
    'use strict';

    angular
        .module('PetWorldModule', [
            'ui.router',
            'angularUtils.directives.dirPagination',
            'ngMessages',
            'ngAnimate',
            'cgNotify',
            'isteven-multi-select',
            'ngImgCrop', 
        ]);

    angular
        .module('PetWorldModule')
        .controller('MainController', MainController);

    MainController.$inject = ['AuthService', '$rootScope'];

    /* @ngInject */
    function MainController(AuthService, $rootScope) {
        AuthService.get().then((response) => {
            $rootScope.Auth = {
                user: response.data.data
            }
        });
    }

    angular
        .module('PetWorldModule')
        .run(NotifyConfig);

    NotifyConfig.$inject = ['notify'];

    function NotifyConfig(notify) {
        notify.config({
            position: 'right',
            duration: '5000', //5 second
            maximumOpen: '5',
            startTop: 75,
            templateUrl: parseView('directives.notify')
        })
    }
})();
