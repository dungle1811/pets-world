(function() {
    'use strict';

    angular
        .module('PetWorldModule')
        .config(RouteConfig);

    RouteConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

    /* @ngInject */
    function RouteConfig($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('dashboard', {
                url: '/',
                template: ''
            })
            .state('pet', {
                abstract: true,
                url: '/pet',
                template: '<ui-view/>'
            })
            .state('pet.list', {
                url: '/list',
                templateUrl: parseView('admin.partials.pets.manage'),
                controller: 'PetController'
            })
            .state('pet.create', {
                url: '/create',
                templateUrl: parseView('admin.partials.pets.create'),
                controller: 'PetManagementController'
            })
            .state('pet.update', {
                url: '/update',
                templateUrl: parseView('admin.partials.pets.create'),
                controller: 'PetManagementController'
            })
            .state('pet.breeds', {
                url: '/breeds',
                templateUrl: parseView('admin.partials.pets.breeds.breeds'),
                controller: 'BreedController'
            })
            .state('pet.status', {
                url: '/status',
                templateUrl: parseView('admin.partials.pets.status.status'),
                controller: 'StatusController'
            })
            .state('pet.types', {
                url: '/types',
                templateUrl: parseView('admin.partials.pets.types.types'),
                controller: 'TypeController'
            })
            .state('user', {
                abstract: true,
                url: '/user',
                template: '<ui-view/>'
            })
            .state('user.profile', {
                url: '/profile',
                templateUrl: parseView('admin.partials.user.profile.index'),
                controller: "UserController"
            })
            .state('user.profile.info', {
                url: '/',
                templateUrl: parseView('admin.partials.user.profile.components.info')
            })
            .state('user.profile.avatar', {
                url: '/avatar',
                templateUrl: parseView('admin.partials.user.profile.components.avatar')
            })
            .state('user.profile.password', {
                url: '/password',
                templateUrl: parseView('admin.partials.user.profile.components.password')
            })
    }

    angular
        .module('PetWorldModule')
        .run(['$rootScope', ($rootScope) => {
            $rootScope.$on('$stateChangeStart', (event, toState, toParams, fromState, fromParams) => {
                $rootScope.isLoading = true;
            });
            $rootScope.$on('$stateChangeSuccess', (event, toState, toParams, fromState, fromParams) => {
                $rootScope.isLoading = false;
            });
        }])
})();
