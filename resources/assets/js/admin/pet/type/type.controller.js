(function() {
    'use strict';

    angular
        .module('PetWorldModule')
        .controller('TypeController', TypeController);

    TypeController.$inject = ['$scope', '$rootScope', 'TypeService', 'notify'];

    /* @ngInject */
    function TypeController($scope, $rootScope, TypeService, notify) {
        $scope.type = {
            name: '',
            description: ''
        }

        /**
         * Get all types
         */
        $scope.types = [];
        TypeService.get().then((response) => {
            $scope.types = response.data.data;
        })

        /**
         * Checklist handle
         */
        $scope.selected_types_value = [];

        $scope.checkAll = () => {
            let selected_status = $scope.isSelectedAll;
            angular.forEach($scope.types, (type) => {
                type.selected = selected_status;
            })
        }

        $scope.uncheckAll = () => {
            $scope.isSelectedAll = false;
            angular.forEach($scope.types, (type) => {
                type.selected = false;
            })
        }

        $scope.changeCheckedOption = () => {
            $scope.isSelectedAll = $scope.types.every((type) => {
                return type.selected;
            })
        }

        $scope.selectedTypes = () => {
            return $filter($scope.types, {
                selected: true
            });
        };

        $scope.$watch('types|filter: {selected: true}', (selected_types) => {
            $scope.length = selected_types.length;

            $scope.selected_types_value = selected_types.map((type) => {
                return type.id;
            })

            let data = {
                type_IDs: $scope.selected_types_value
            }

            // Delete multiple types
            $scope.deleteMultiple = () => {
                TypeService.deleteMultiple(data).then((response) => {
                    $scope.types = $scope.types.filter((type) => {
                        let count = 0;
                        for (let i = 0; i < response.data.data.length; i++) {
                            if (type.id != response.data.data[i]) {
                                count++;
                            }
                            if (count == response.data.data.length) {
                                return type;
                            }
                        }
                    });
                    $scope.isSelectedAll = false;
                    notify({
                        message: response.data.message,
                        classes: 'alert alert-success'
                    });
                },
                (response) => {
                    notify({
                        message: response.data.message,
                        classes: 'alert alert-danger'
                    });
                })
            }
        }, true);

        /**
         * Create Feature
         */
        $scope.create = (type) => {
            $rootScope.isLoading = true;
            TypeService.create(type).then((response) => {
                    $scope.updateSpecifications(response.data.data.id);
                    $scope.types.push(response.data.data);
                    $scope.checkAll();
                    $scope.changeCheckedOption();
                    $scope.reset();
                    notify({
                        message: response.data.message,
                        classes: 'alert alert-success',
                        duration: '2000'
                    });
                },
                (response) => {
                    $scope.setNameError(response.data.data.name[0]);
                    notify({
                        message: response.data.message,
                        classes: 'alert alert-danger'
                    });
                }).finally(() => {
                $rootScope.isLoading = false;
                $scope.setPristine();
            })
        };

        $scope.updateSpecifications = (type_id) => {
            //Filter empty specifications then convert to database entity field
            let data = $scope.specifications
                .map((spec) => {
                    var specification = {
                        name: spec.name,
                        unit_id: spec.unit.id,
                        unit_type_id: spec.type.id,
                        pet_type_id: type_id
                    }
                    return specification;
                });
            TypeService.saveSpecifications({
                specifications: data
            }).then((response) => {
                angular.forEach($scope.types, function(type) {
                    if (type.id == type_id) {
                        type.specifications = data;
                    }
                })
            });
        }

        $scope.reset = () => {
            let type = {
                name: '',
                description: '',
            };
            $scope.type = angular.copy(type);
            $scope.specifications = [getDefaultSpecification()];
            $scope.setPristine();
        };

        //  Get innitial type
        $scope.getOne = (type) => {
            let getType = TypeService.getOne($scope.types, type.id);
            let oldType = {
                id: getType.id,
                name: getType.name,
                description: getType.description,
                specifications: getType.specifications
            }
            return oldType;
        };

        /**
         * Update Feature
         */
        $scope.setUpdate = (type) => {
            $scope.isUpdate = true;
            $scope.type = $scope.getOne(type);
            $scope.specifications = $scope.type.specifications;
            angular.forEach($scope.specifications, (spec) => {
                angular.forEach($scope.unit_types, (type) => {
                    if (spec.unit_type_id == type.id) {
                        spec.type = type;
                    }
                })
                angular.forEach(spec.type.units, (unit) => {
                    if (spec.unit_id == unit.id) {
                        spec.unit = unit;
                    }
                })
            })
        };
        $scope.update = (typeUpdated) => {
            $rootScope.isLoading = true;
            TypeService.update(typeUpdated).then((response) => {
                    $scope.updateSpecifications(response.data.data.id);
                    $scope.types = $scope.types.map((type) => {
                        if (type.id == response.data.data.id) {
                            type = response.data.data;
                        }
                        return type;
                    });
                    $scope.reset();
                    $scope.isUpdate = false;
                    notify({
                        message: response.data.message,
                        classes: 'alert alert-success',
                        duration: '2000'
                    });
                },
                (response) => {
                    $scope.isUpdate = true;
                    console.log(response.data.data);
                    $scope.setNameError(response.data.data.name[0]);
                    notify({
                        message: response.data.message,
                        classes: 'alert alert-danger'
                    });
                }).finally(() => {
                $scope.setPristine();
                $rootScope.isLoading = false;
            })
        }

        $scope.setNameError = (error) => {
            $scope.existedNameError = error;
            $scope.isExistedName = true;
        }

        $scope.resetNameError = () => {
            $scope.isExistedName = false
            $scope.existedNameError = '';
        }

        $scope.cancel = () => {
            $scope.isUpdate = false;
            $scope.reset();
        };

        /**
         * Delete Feature
         */
        $scope.setDelete = (type) => {
            $scope.typeDeleted = $scope.getOne(type);
        };
        $scope.delete = (typeDeleted) => {
            $rootScope.isLoading = true;
            TypeService.delete(typeDeleted).then((response) => {
                    $scope.types = $scope.types.filter((type) => {
                        return type.id != response.data.data.id
                    })
                    notify({
                        message: response.data.message,
                        classes: 'alert alert-success',
                        duration: '2000'
                    });
                },
                (response) => {
                    notify({
                        message: response.data.message,
                        classes: 'alert alert-danger'
                    });
                }).finally(() => {
                $rootScope.isLoading = false;
            })
        };

        /**
         * Search Feature
         */
        $scope.resetFilter = () => {
            $scope.searchText = '';
        }

        /**
         * Reset validation
         */
        $scope.setPristine = () => {
            $scope.form.submitForm.$setPristine();
            $scope.form.submitForm.$setUntouched();
        }

        /**
         * Pet specification
         */
        $scope.specifications = [{}];

        let getDefaultSpecification = function() {
            return {
                type: $scope.unit_types[0],
                unit: $scope.unit_types[0].units[0],
                name: ''
            }
        }

        TypeService.getAllUnit().then((response) => {
            $scope.unit_types = response.data.data;
            $scope.specifications = [getDefaultSpecification()];
        })

        $scope.addSpec = () => {
            $scope.specifications.push(getDefaultSpecification());
        }

        $scope.removeSpec = (index) => {
            $scope.specifications.splice(index, 1);
        }
    }
})();
