(function() {
    'use strict';

    angular
        .module('PetWorldModule')
        .service('TypeService', TypeService);

    TypeService.$inject = ['$authHttp'];

    /* @ngInject */
    function TypeService($http) {
        this.get = () => {
            return $http.get('/api/pet-types/all');
        };
        this.getOne = (types, typeID) => {
            let type = types.filter((type) => {
                return type.id == typeID;
            })
            return type[0];
        }
        this.getAllUnit = () => {
            return $http.get('/api/pet-unit-types/all');
        }
        this.create = (type) => {
            return $http.post('/api/pet-types/create', type);
        };
        this.saveSpecifications = (data) => {
            return $http.post('/api/pet-specifications/save', data);
        }
        this.update = (type) => {
            return $http.post('/api/pet-types/update', type);
        };
        this.delete = (type) => {
            return $http.post('/api/pet-types/delete', type);
        };
        this.deleteMultiple = (type_IDs) => {
            return $http.post('/api/pet-types/delete/multiple', type_IDs);
        };
    }
})();
