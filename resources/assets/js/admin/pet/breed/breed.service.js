(function() {
    'use strict';

    angular
        .module('PetWorldModule')
        .service('BreedService', BreedService);

    BreedService.$inject = ['$authHttp', '$formDataHttp'];

    /* @ngInject */
    function BreedService($http, $formDataHttp) {
        this.get = () => {
            return $http.get('/api/pet-breeds/all');
        };
        this.getOne = (breeds, breedID) => {
            let breed = breeds.filter((breed) => {
                return breed.id == breedID;
            })
            return breed[0];
        }
        this.getByType = (breeds, typeID) => {
            let breedsArr = angular.copy(breeds);
            let breedsByType = breedsArr.filter((breed) => {
                return breed.type_id == typeID;
            })
            return breedsByType;
        }
        this.create = (breed) => {
            return $formDataHttp.post('/api/pet-breeds/create', breed);
        };
        this.update = (breed) => {
            return $formDataHttp.post('/api/pet-breeds/update', breed);
        };
        this.delete = (breed) => {
            return $http.post('/api/pet-breeds/delete', breed);
        };
        this.deleteMultiple = (breed_IDs) => {
            return $http.post('/api/pet-breeds/delete/multiple', breed_IDs);
        };
    }
})();
