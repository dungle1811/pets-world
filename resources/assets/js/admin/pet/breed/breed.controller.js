(function() {
    'use strict';

    angular
        .module('PetWorldModule')
        .controller('BreedController', BreedController);

    BreedController.$inject = ['$scope', '$rootScope', '$timeout', '$filter', 'BreedService', 'TypeService', 'notify'];

    /* @ngInject */
    function BreedController($scope, $rootScope, $timeout, $filter, BreedService, TypeService, notify) {
        $scope.breed = {
            name: '',
            description: '',
            type_id: null,
        }

        $scope.breeds = [];
        BreedService.get().then((response) => {
            $scope.breeds = response.data.data;
        })

        $scope.types = [];
        TypeService.get().then((response) => {
            $scope.types = response.data.data;
        })

        /**
         * Filter by breed
         */
        $scope.default_option = 'All';
        $scope.$watch('typeOption', (typeOption) => {
            if (typeOption == null) {
                $scope.typeOption = {};
            }
        })

        /**
         * Checklist handle
         */
        $scope.selected_breeds_value = [];

        $scope.checkAll = () => {
            let selected_status = $scope.isSelectedAll;
            angular.forEach($scope.breeds, (breed) => {
                breed.selected = selected_status;
            })
        }

        $scope.uncheckAll = () => {
            $scope.isSelectedAll = false;
            angular.forEach($scope.breeds, (breed) => {
                breed.selected = false;
            })
        }

        $scope.changeCheckedOption = () => {
            $scope.isSelectedAll = $scope.breeds.every((breed) => {
                return breed.selected;
            })
        }

        $scope.selectedBreeds = () => {
            return $filter($scope.breeds, {
                selected: true
            });
        };

        $scope.$watch('breeds|filter: {selected: true}', (selected_breeds) => {
            $scope.length = selected_breeds.length;

            $scope.selected_breeds_value = selected_breeds.map((breed) => {
                return breed.id;
            })

            let data = {
                breed_IDs: $scope.selected_breeds_value
            }

            // Delete multiple breeds
            $scope.deleteMultiple = () => {
                BreedService.deleteMultiple(data).then((response) => {
                    $scope.breeds = $scope.breeds.filter((breed) => {
                        let count = 0;
                        for (let i = 0; i < response.data.data.length; i++) {
                            if (breed.id != response.data.data[i]) {
                                count++;
                            }
                            if (count == response.data.data.length) {
                                return breed;
                            }
                        }
                    });
                    $scope.isSelectedAll = false;
                    notify({
                        message: response.data.message,
                        classes: 'alert alert-success',
                        duration: '2000'
                    });
                },
                (response) => {
                    notify({
                        message: response.data.message,
                        classes: 'alert alert-danger',
                    });
                })
            }
        }, true);


        /**
         * Create Feature
         */
        $scope.create = (breed) => {
            $rootScope.isLoading = true;
            let breedCreated = {
                name: breed.name,
                description: breed.description,
                type_id: breed.type_id,
                breed_photo: breed.photo
            }
            BreedService.create(breedCreated).then((response) => {
                    $scope.reset();
                    $scope.breeds.push(response.data.data);
                    $scope.checkAll();
                    $scope.changeCheckedOption();
                    notify({
                        message: response.data.message,
                        classes: 'alert alert-success',
                        duration: '2000'
                    });
                },
                (response) => {
                    console.log(response.data.data);
                    $scope.setNameError(response.data.data.name[0]);
                }).finally(() => {
                $rootScope.isLoading = false;
                $scope.setPristine();
            })
        };

        $scope.reset = () => {
            let breed = {
                name: ''
            };
            $scope.breed = angular.copy(breed);
            $scope.setPristine();
        };

        //  Get innitial breed
        $scope.getOne = (breed) => {
            let getBreed = BreedService.getOne($scope.breeds, breed.id);
            let oldBreed = {
                id: getBreed.id,
                name: getBreed.name,
                description: getBreed.description,
                type_id: getBreed.type_id,
                breed_photo: getBreed.photo
            }
            return oldBreed;
        };

        /**
         * Update Feature
         */
        $scope.setUpdate = (breed) => {
            $scope.isUpdate = true;
            let getBreed = $scope.getOne(breed);
            $scope.breed = getBreed;
            $scope.breed.photo = getBreed.breed_photo;

            $timeout(() => {
                $('#type').val(getBreed.type_id);
            })
        };
        $scope.update = (updatedBreed) => {
            $rootScope.isLoading = true;
            let breed = {
                id: updatedBreed.id,
                name: updatedBreed.name,
                description: updatedBreed.description,
                type_id: updatedBreed.type_id,
                breed_photo: updatedBreed.photo
            }
            BreedService.update(breed).then((response) => {
                    $scope.breeds = $scope.breeds.map((breed) => {
                        if (breed.id == response.data.data.id) {
                            breed = response.data.data;
                        }
                        return breed;
                    });
                    $scope.reset();
                    $scope.isUpdate = false;
                    notify({
                        message: response.data.message,
                        classes: 'alert alert-success',
                        duration: '2000'
                    });
                },
                (response) => {
                    $scope.isUpdate = true;
                    $scope.setNameError(response.data.data);
                    notify({
                        message: response.data.message,
                        classes: 'alert alert-danger'
                    });
                }).finally(() => {
                $scope.setPristine();
                $rootScope.isLoading = false;
            })
        }

        $scope.setNameError = (error) => {
            $scope.existedNameError = error;
            $scope.isExistedName = true;
        }

        $scope.resetNameError = () => {
            $scope.isExistedName = false
            $scope.existedNameError = '';
        }

        $scope.cancel = () => {
            $scope.isUpdate = false;
            $scope.reset();
        };

        /**
         * Delete Feature
         */
        $scope.setDelete = (breed) => {
            $scope.breedDeleted = $scope.getOne(breed);
        };
        $scope.delete = (breedDeleted) => {
            $rootScope.isLoading = true;
            BreedService.delete(breedDeleted).then((response) => {
                    $scope.breeds = $scope.breeds.filter((breed) => {
                        return breed.id != response.data.data.id
                    })
                    notify({
                        message: response.data.message,
                        classes: 'alert alert-success',
                        duration: '2000'
                    });
                },
                (response) => {
                    console.log(response.data.data);
                    notify({
                        message: response.data.message,
                        classes: 'alert alert-danger'
                    });
                }).finally(() => {
                $rootScope.isLoading = false;
            })
        };

        /**
         * Search Feature
         */
        $scope.resetFilter = () => {
            $scope.searchText = '';
            $scope.typeOption = {};
        }

        /**
         * Reset validation
         */
        $scope.setPristine = () => {
            $scope.form.submitForm.$setPristine();
            $scope.form.submitForm.$setUntouched();
        }
    }
})();
