(function() {
    'use strict';

    angular
        .module('PetWorldModule')
        .service('StatusService', StatusService);

    StatusService.$inject = ['$authHttp'];

    /* @ngInject */
    function StatusService($http) {
        this.get = () => {
            return $http.get('/api/pet-status/all');
        };
        this.getOne = (status, statusID) => {
            let statusChild = status.filter((statusChild) => {
                return statusChild.id == statusID;
            })
            return statusChild[0];
        }
        this.create = (statusChild) => {
            return $http.post('/api/pet-status/create', statusChild);
        };
        this.update = (statusChild) => {
            return $http.post('/api/pet-status/update', statusChild);
        };
        this.delete = (statusChild) => {
            return $http.post('/api/pet-status/delete', statusChild);
        };
        this.deleteMultiple = (statusChild_IDs) => {
            return $http.post('/api/pet-status/delete/multiple', statusChild_IDs);
        };
    }
})();
