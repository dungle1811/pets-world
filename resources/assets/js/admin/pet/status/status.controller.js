(function() {
    'use strict';

    angular
        .module('PetWorldModule')
        .controller('StatusController', StatusController);

    StatusController.$inject = ['$scope', '$rootScope', 'StatusService', 'notify'];

    /* @ngInject */
    function StatusController($scope, $rootScope, StatusService, notify) {
        $scope.statusChild = {
            name: '',
            description: ''
        }
        /**
         * Get all status
         */
        $scope.status = [];
        StatusService.get().then((response) => {
            $scope.status = response.data.data;
        })

        /**
         * Checklist handle
         */
        $scope.selected_status_value = [];

        $scope.checkAll = () => {
            let selected_status = $scope.isSelectedAll;
            angular.forEach($scope.status, (statusChild) => {
                statusChild.selected = selected_status;
            })
        }

        $scope.uncheckAll = () => {
            $scope.isSelectedAll = false;
            angular.forEach($scope.status, (statusChild) => {
                statusChild.selected = false;
            })
        }

        $scope.changeCheckedOption = () => {
            $scope.isSelectedAll = $scope.status.every((statusChild) => {
                return statusChild.selected;
            })
        }

        $scope.selectedStatus = () => {
            return $filter($scope.status, {
                selected: true
            });
        };

        $scope.$watch('status|filter: {selected: true}', (selected_status) => {
            $scope.length = selected_status.length;

            $scope.selected_status_value = selected_status.map((statusChild) => {
                return statusChild.id;
            })

            let data = {
                statusChild_IDs: $scope.selected_status_value
            }

            // Delete multiple status
            $scope.deleteMultiple = () => {
                StatusService.deleteMultiple(data).then((response) => {
                    $scope.status = $scope.status.filter((statusChild) => {
                        let count = 0;
                        for (let i = 0; i < response.data.data.length; i++) {
                            if (statusChild.id != response.data.data[i]) {
                                count++;
                            }
                            if (count == response.data.data.length) {
                                return statusChild;
                            }
                        }
                    });
                    $scope.isSelectedAll = false;
                })
            }
        }, true);

        /**
         * Create Feature
         */
        $scope.create = (statusChild) => {
            $rootScope.isLoading = true;
            StatusService.create(statusChild).then((response) => {
                    $scope.reset();
                    $scope.status.push(response.data.data);

                    $scope.checkAll();
                    $scope.changeCheckedOption();
                    notify({
                        message: response.data.message,
                        classes: 'alert alert-success',
                        duration: '2000'
                    });
                },
                (response) => {
                    console.log(response.data.data);
                    $scope.setNameError(response.data.data.name[0]);
                    notify({
                        message: response.data.message,
                        classes: 'alert alert-danger'
                    });
                }).finally(() => {
                $rootScope.isLoading = false;
                $scope.setPristine();
            })
        };
        $scope.reset = () => {
            let statusChild = {
                name: ''
            };
            $scope.statusChild = angular.copy(statusChild);
            $scope.setPristine();
            $scope.errorNotification = '';
        };

        //  Get innitial statusChild
        $scope.getOne = (statusChild) => {
            let getStatusChild = StatusService.getOne($scope.status, statusChild.id);
            let oldStatusChild = {
                id: getStatusChild.id,
                name: getStatusChild.name,
                description: getStatusChild.description
            }
            return oldStatusChild;
        };

        /**
         * Update Feature
         */
        $scope.setUpdate = (statusChild) => {
            $scope.isUpdate = true;
            $scope.statusChild = $scope.getOne(statusChild);
        };
        $scope.update = (statusChildUpdated) => {
            $rootScope.isLoading = true;
            StatusService.update(statusChildUpdated).then((response) => {
                    $scope.status = $scope.status.map((statusChild) => {
                        if (statusChild.id == response.data.data.id) {
                            statusChild = response.data.data;
                        }
                        return statusChild;
                    });
                    $scope.reset();
                    $scope.isUpdate = false;
                    notify({
                        message: response.data.message,
                        classes: 'alert alert-success',
                        duration: '2000'
                    });
                },
                (response) => {
                    $scope.isUpdate = true;
                    $scope.setNameError(response.data.data);
                    notify({
                        message: response.data.message,
                        classes: 'alert alert-danger'
                    });
                }).finally(() => {
                $rootScope.isLoading = false;
                $scope.setPristine();
            })
        }

        $scope.setNameError = (error) => {
            $scope.existedNameError = error;
            $scope.isExistedName = true;
        }

        $scope.resetNameError = () => {
            $scope.isExistedName = false
            $scope.existedNameError = '';
        }

        $scope.cancel = () => {
            $scope.isUpdate = false;
            $scope.reset();
        };

        /**
         * Delete Feature
         */
        $scope.setDelete = (statusChild) => {
            $scope.statusChildDeleted = $scope.getOne(statusChild);
        };
        $scope.delete = (statusChildDeleted) => {
            $rootScope.isLoading = true;
            StatusService.delete(statusChildDeleted).then((response) => {
                    $scope.status = $scope.status.filter((statusChild) => {
                        return statusChild.id != response.data.data.id
                    });
                    notify({
                        message: response.data.message,
                        classes: 'alert alert-success',
                        duration: '2000'
                    });
                },
                (response) => {
                    console.log(response.data.data);
                    notify({
                        message: response.data.message,
                        classes: 'alert alert-danger'
                    });
                }).finally(() => {
                $rootScope.isLoading = false;
            })
        };

        /**
         * Search Feature
         */
        $scope.resetFilter = () => {
            $scope.searchText = '';
        }

        /**
         * Reset validation
         */
        $scope.setPristine = () => {
            $scope.form.submitForm.$setPristine();
            $scope.form.submitForm.$setUntouched();
        }
    }
})();
