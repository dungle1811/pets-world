(function() {
    'use strict';

    angular
        .module('PetWorldModule')
        .service('PetService', PetService);

    PetService.$inject = ['$q', '$authHttp', '$formDataHttp'];

    /* @ngInject */
    function PetService($q, $http, $formDataHttp) {
        this.types = [];
        this.breeds = [];
        this.status = [];
        this.pets = [];

        this.getModel = (model, modelDefered, api_path) => {
            if (model.length != 0) {
                modelDefered.resolve(model);
            } else {
                $http.get(api_path)
                    .then((response) => {
                        model = response.data.data;
                        modelDefered.resolve(model);
                    })
                    .then((response) => {
                        modelDefered.reject(response);
                    })
            }
        }

        // Infos include: types, breeds, status
        // this function returns a promise
        this.getInfos = () => {
            // create defered operation
            let typesDefered = $q.defer();
            let breedsDefered = $q.defer();
            let statusDefered = $q.defer();
            let petsDefered = $q.defer();

            let get_types_api_path = '/api/pet-types/all';
            let get_breeds_api_path = '/api/pet-breeds/all';
            let get_status_api_path = '/api/pet-status/all';
            let get_pets_api_path = '/api/pets/all';

            // if types already exists, resolve the types
            // else get types from API
            this.getModel(this.types, typesDefered, get_types_api_path);
            this.getModel(this.breeds, breedsDefered, get_breeds_api_path);
            this.getModel(this.status, statusDefered, get_status_api_path);
            this.getModel(this.pets, petsDefered, get_pets_api_path);

            return $q.all([typesDefered.promise, breedsDefered.promise, statusDefered.promise, petsDefered.promise]);
        }

        this.create = (pet) => {
            return $formDataHttp.post('/api/pets/create', pet);
        }

        this.updateLandscapePhoto = (data) => {
            return $formDataHttp.post('/api/pets/update/photo/landscape', data);
        }

        this.updateSquarePhoto = (data) => {
            return $formDataHttp.post('/api/pets/update/photo/square', data);
        }

        this.update = (pet) => {
            return $formDataHttp.post('/api/pets/update', pet);
        }

        this.delete = (pet) => {
            return $http.post('/api/pets/delete', pet);
        }
    }
})();
