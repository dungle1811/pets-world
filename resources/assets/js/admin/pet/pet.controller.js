(function() {
    'use strict';

    angular
        .module('PetWorldModule')
        .controller('PetController', PetController);

    PetController.$inject = [
        '$rootScope', '$scope', '$location', '$state', 'notify',
        'TypeService', 'BreedService', 'StatusService', 'PetService'
    ];

    /* @ngInject */
    function PetController($rootScope, $scope, $location, $state, notify, TypeService, BreedService, StatusService, PetService) {
        $scope.pets = [];
        $scope.isLoading = true;

        /**
         * Bind data to list view
         */
        $scope.bindToList = (pets) => {
            angular.forEach(pets, (pet) => {
                if (pet.sex == 0) {
                    pet.isMale = true;
                } else pet.isMale = false;
            })
        }

        // call promise from service
        PetService.getInfos().then((responses) => {
            // Get all breeds
            $scope.types = responses[0];

            // Get all pets
            $scope.pets = responses[3];

            $scope.bindToList($scope.pets);
            $scope.setActiveAttr();

            $scope.isLoading = false;
        });

        /**
         * Search and filter
         */
        $scope.resetingActive = true;

        $scope.resetFilter = () => {
            $scope.searchText = '';
        }

        $scope.setActiveAttr = () => {
            angular.forEach($scope.types, (type) => {
                type.active = false;
            });
        }

        $scope.filterByType = (type) => {
            $scope.typeOption = type.name;
            $scope.setActiveAttr();
            type.active = true;
            $scope.resetingActive = false;
        }

        $scope.resetFilterByType = () => {
            $scope.setActiveAttr();
            $scope.typeOption = '';
            $scope.resetingActive = true;
        }

        /**
         * Update pet
         */
        $scope.setUpdate = (pet) => {
            $rootScope.petToUpdate = pet;
            $state.go('pet.update');
        }

        /**
         * Delete pet
         */
        $scope.setDelete = (pet) => {
            $scope.deletedPet = {
                id: pet.id,
                name: pet.name
            };
        };

        $scope.delete = (deletedPet) => {
            $rootScope.isLoading = true;
            PetService.delete(deletedPet).then((response) => {
                    $scope.pets = $scope.pets.filter((pet) => {
                        return pet.id != deletedPet.id
                    })
                    notify({
                        message: response.data.message,
                        classes: 'alert alert-success',
                        duration: '2000'
                    });
                },
                (response) => {
                    console.log(response.data.data);
                    notify({
                        message: response.data.message,
                        classes: 'alert alert-danger'
                    });
                }).finally(() => {
                $rootScope.isLoading = false;
            })
        }

        /**
         * Pagination
         */
        $scope.quantities = [12, 24, 50, 80, 100];
        $scope.dirPaginateTemplate = parseView('directives.dir-pagination');
        $scope.viewBy = 12;
        $scope.currentPage = 1;
        $scope.pageSize = $scope.viewBy;

        // Handle view by quantity
        $scope.setItemsPerPage = () => {
            $scope.pageSize = $scope.viewBy;
            $scope.currentPage = 1;
        }
    }
})();
