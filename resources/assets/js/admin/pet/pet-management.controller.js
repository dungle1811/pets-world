(function() {
    'use strict';

    angular
        .module('PetWorldModule')
        .controller('PetManagementController', PetManagementController);

    PetManagementController.$inject = ['$rootScope', '$scope', '$state', 'TypeService', 'BreedService', 'StatusService', 'PetService', 'notify'];

    /* @ngInject */
    function PetManagementController($rootScope, $scope, $state, TypeService, BreedService, StatusService, PetService, notify) {
        // Change lable
        $scope.langForOne = {
            nothingSelected: 'Choose one',
            reset: 'Reset',
            search: 'Search...'
        }

        $scope.langForMore = {
            nothingSelected: 'Choose one or more',
            reset: 'Reset',
            search: 'Search...',
            selectAll: 'Select all',
            selectNone: 'Select none'
        }

        // Initialize pet
        $scope.pet = {
            sex: 0
        }

        // Initialize genders
        $scope.genders = [{
                id: 1,
                name: 'Male',
                bool: 0
            },
            {
                id: 2,
                name: 'Female',
                bool: 1
            }
        ];

        // Handle Select box
        $scope.isShowBreeds = false;
        $scope.isShowUnitProperties = false;

        // Show unit properties
        $scope.showUnits = () => {
            $scope.isShowUnitProperties = true;
        }

        // Hide unit properties
        $scope.hideUnits = () => {
            $scope.isShowUnitProperties = false;
        }

        // Show breeds select box when select one type
        $scope.clickTypeFunc = () => {
            $scope.isShowBreeds = true;
            $scope.inputBreeds = BreedService.getByType($scope.breeds, $scope.selectedType[0].id);
            $scope.showUnits();
        }

        // Hide breeds and status select box when reset type
        $scope.resetTypeFunc = () => {
            angular.forEach($scope.inputBreeds, (breed) => {
                breed.selected = false;
            })
            $scope.isShowBreeds = false;

            $scope.resetStatusFunc();
            $scope.hideUnits();
        }

        // Reset status options
        $scope.resetStatusFunc = () => {
            angular.forEach($scope.inputStatus, (statusChild) => {
                statusChild.selected = false;
            })
        }

        $scope.isUpdate = false;

        if ($state.current.name == 'pet.update') {
            if ($rootScope.petToUpdate) {
                $scope.isUpdate = true;
            } else {
                $state.go('pet.create');
            }
        }

        $scope.isLoading = true;

        // call promise from service
        PetService.getInfos().then((responses) => {
            $scope.types = responses[0];
            $scope.breeds = responses[1];
            $scope.status = responses[2];

            // Get types
            $scope.inputTypes = $scope.types;

            // Get breeds
            $scope.inputBreeds = $scope.breeds;

            // Get status
            $scope.inputStatus = $scope.status;

            // Get all pets
            $scope.pets = responses[3];

            $scope.clickTypeFunc = () => {
                $scope.isShowBreeds = true;
                $scope.inputBreeds = BreedService.getByType($scope.breeds, $scope.selectedType[0].id);
                $scope.showUnits();
            }

            $scope.resetTypeFunc();
            $scope.resetStatusFunc();

            $scope.setUpdate();

        });

        // Info: type, breed, height, weight, status, price
        $scope.showInfo = (typeID) => {
            $scope.isShowBreeds = true;
            $scope.inputBreeds = BreedService.getByType($scope.breeds, typeID);
            $scope.showUnits();
        }

        $scope.setOptions = (selectedOptions, initOptions) => {
            angular.forEach(initOptions, (option) => {
                if (option.id == selectedOptions.id) {
                    option.selected = true;
                }
            })
        }

        $scope.resetOptions = (initOptions) => {
            angular.forEach(initOptions, (option) => {
                option.selected = false;
            })
        }

        // Update photo function
        $scope.updatePhoto = (petID, landscapePhoto, squarePhoto) => {
            // Update landscape photo
            PetService.updateLandscapePhoto({
                id: petID,
                photo: landscapePhoto
            }).then((response) => {})

            // Update square photo
            PetService.updateSquarePhoto({
                id: petID,
                photo: squarePhoto
            }).then((response) => {})
        }

        // Set pet to update form
        if ($rootScope.petToUpdate) {
            $scope.pet = $rootScope.petToUpdate;
        }
        $scope.setUpdate = () => {
            if ($rootScope.petToUpdate) {
                $scope.pet = $rootScope.petToUpdate;

                delete $rootScope.petToUpdate;

                // Set type
                let typeOfPet = $scope.pet.pet_type;
                $scope.setOptions(typeOfPet, $scope.inputTypes);

                $scope.showInfo(typeOfPet.id);

                // Set breed
                let breedOfPet = $scope.pet.pet_breed;
                $scope.setOptions(breedOfPet, $scope.inputBreeds);

                // Set status
                let statusOfPet = $scope.pet.pet_status;
                angular.forEach($scope.inputStatus, (statusChild) => {
                    angular.forEach(statusOfPet, (data) => {
                        if (statusChild.id == data.id) {
                            statusChild.selected = true;
                        }
                    })
                });
            }
        }

        // Handle image cropper showing
        if ($scope.isUpdate) {
            $scope.isChangePhoto = true;
            $scope.isShowPhotoCropper = false;
            $scope.showPhotoCropper = () => {
                $scope.isShowPhotoCropper = true;
                $scope.isChangePhoto = false;
            }
        } else {
            $scope.isShowPhotoCropper = true;
        }

        /**
         * Update pet
         */
        $scope.update = (pet) => {
            $rootScope.isLoading = true;

            // Get selected status
            let selectedStatusArr = [];
            angular.forEach($scope.selectedStatus, (statusChild) => {
                selectedStatusArr.push(statusChild.id);
            });

            let updatedPet = {
                id: pet.id,
                name: pet.name,
                description: pet.description,
                sex: parseInt(pet.sex),
                type_id: $scope.selectedType[0].id,
                breed_id: $scope.selectedBreed[0].id,
                weight: pet.weight,
                height: pet.height,
                pet_status: selectedStatusArr,
                price: pet.price
            };

            let photo_landscape = pet.photo_landscape;
            let photo_square = pet.photo_square;

            PetService.update(updatedPet).then((response) => {
                    let pet = response.data.data;

                    // Update photo
                    $scope.updatePhoto(pet.id, photo_landscape, photo_square);

                    notify({
                        message: response.data.message,
                        classes: 'alert alert-success',
                        duration: '2000'
                    });

                    $scope.reset();

                    $state.go('pet.list');
                },
                (response) => {
                    $scope.setNameError(response.data.data);
                    notify({
                        message: response.data.message,
                        classes: 'alert alert-danger'
                    });
                    $scope.isUpdate = true;
                }).finally(() => {
                $rootScope.isLoading = false;
                $scope.setPristine();
            })
        }

        $scope.resetNameError = () => {
            $scope.isExistedName = false
            $scope.existedNameError = '';
        }

        /**
         * Create pet
         */
        $scope.create = (pet) => {
            $rootScope.isLoading = true;
            // Get selected status
            let selectedStatusArr = [];
            angular.forEach($scope.selectedStatus, (statusChild) => {
                selectedStatusArr.push(statusChild.id);
            })

            let createdPet = {
                name: pet.name,
                description: pet.description,
                sex: parseInt(pet.sex),
                type_id: $scope.selectedType[0].id,
                breed_id: $scope.selectedBreed[0].id,
                weight: pet.weight,
                height: pet.height,
                pet_status: selectedStatusArr,
                price: pet.price
            };

            let photo_landscape = pet.photo_landscape;
            let photo_square = pet.photo_square;

            PetService.create(createdPet).then((response) => {
                    let pet = response.data.data;

                    // $scope.pets.push(pet);

                    // Update photo
                    $scope.updatePhoto(pet.id, photo_landscape, photo_square);

                    notify({
                        message: response.data.message,
                        classes: 'alert alert-success',
                        duration: '2000'
                    });

                    $scope.reset();
                },
                (response) => {
                    $scope.setNameError(response.data.data.name[0]);
                    notify({
                        message: response.data.message,
                        classes: 'alert alert-danger'
                    });
                }).finally(() => {
                $rootScope.isLoading = false;
                $scope.setPristine();
            })
        }

        $scope.setNameError = (error) => {
            $scope.existedNameError = error;
            $scope.isExistedName = true;
        }

        // Cancel update
        $scope.cancel = () => {
            $state.go('pet.create');
        }

        // Reset form
        $scope.reset = () => {
            let pet = {
                name: '',
                sex: 0,
            }
            angular.forEach($scope.inputTypes, (type) => {
                type.selected = false;
            })
            $scope.resetTypeFunc();
            $scope.resetStatusFunc();
            $scope.pet = angular.copy(pet);
        }

        $scope.setPristine = () => {
            $scope.form.submitForm.$setPristine();
            $scope.form.submitForm.$setUntouched();
        }
    }
})();
