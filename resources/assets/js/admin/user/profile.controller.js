(function() {
	'use strict';

	angular
	  .module('PetWorldModule')
	  .controller('UserController', UserController);

	UserController.$inject = ['$rootScope', '$scope', 'AuthService', 'notify'];

	function UserController($rootScope, $scope, AuthService, notify) {
		$scope.$watch('Auth', (Auth) => {
			$scope.user = angular.copy(Auth.user);
		})

		$scope.updateInfo = () => {
			AuthService.updateInfo($scope.user).then((response) => {
				$rootScope.Auth.user = response.data.data;
                notify({
                    message: response.data.message,
                    classes: 'alert alert-success',
                    duration: '2000'
                });
			})
		}
	}
})();