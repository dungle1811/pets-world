(function() {
	'use strict';

	angular
	  .module('PetWorldModule')
	  .service('AuthService', AuthService);

	AuthService.$inject = ['$authHttp', '$rootScope'];

	function AuthService($http, $rootScope) {
		this.get = () => {
			return $http.get('api/auth/get');
		}

		this.updateInfo = (data) => {
			return $http.post('api/auth/update', data)
		}
	}
})();