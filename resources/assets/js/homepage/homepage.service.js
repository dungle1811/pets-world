(function() {
	'use strict';

	angular
	  .module('PetWorldModule')
	  .service('HomepageService', HomepageService);

	HomepageService.$inject = ['$http'];

	function HomepageService($http) {
		this.getAllPets = () => {
			return $http.get('/api/pets/all');
		};

		this.getAllBreeds = () => {
			return $http.get('/api/pet-breeds/all');
		};

		this.getBreedsByType = (breeds, typeID) => {
			let breedsArr = angular.copy(breeds);
			let breedsByType = breedsArr.filter((breed) => {
				return breed.type_id == typeID;
			});
			return breedsByType;
		}
	}
})();