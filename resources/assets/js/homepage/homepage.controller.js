(function() {
	'use strict';

	angular
	  .module('PetWorldModule')
	  .controller('HomepageController', HomepageController);

	HomepageController.$inject = ['$scope', 'HomepageService'];

	function HomepageController($scope, HomepageService) {
		$scope.breeds = [];
		$scope.breedsByType = [];

		HomepageService.getAllPets().then((response) => {
			$scope.pets = response.data.data;
		})

		$scope.getBreedsByType = (typeID) => {
			HomepageService.getAllBreeds().then((response) => {
				$scope.breeds = response.data.data;
				$scope.breedsByType = HomepageService.getBreedsByType($scope.breeds, typeID);
				let allBreeds = {
					name: 'All',
					isAll: true,
					pet_type: $scope.breedsByType[0].pet_type
				};
				$scope.breedsByType.unshift(allBreeds);
			})
		}

		$scope.getBreedName = (breed) => {
			$scope.breedNameFilter = breed.name;
			if (breed.isAll) {
				$scope.typeNameFilter = breed.pet_type.name;
				$scope.breedNameFilter = '';
				breed.isActive = true;

				angular.forEach($scope.breedsByType, (pet_breed) => {
					if (pet_breed.id != breed.id) {
						pet_breed.isActive = false;
					}
				})

				$('.no-data').empty();

				return;
			};

			angular.forEach($scope.breedsByType, (pet_breed) => {
				if (pet_breed.name == $scope.breedNameFilter) {
					pet_breed.isActive = true;
				} else pet_breed.isActive = false;
			});


			let pets = $scope.pets.filter((petFilter) => {
				return petFilter.pet_breed.id == breed.id;
			})

			if (pets.length == 0) {
				$('.no-data').html('<span>No pets here.</span>');
			} else $('.no-data').empty();
		}

		$('.type-btn').on('click', function () {
			if ($(this).attr('data-target')) {
				let clickedBtn = $(this).attr('data-target').slice(1);
				$('.pet-types-collapse').each(function() {
					if ($(this).attr('id') != clickedBtn){
						$(this).collapse('hide');
					}
				});
			}
		});

		$scope.resetPetsFilter = () => {
			$scope.breedNameFilter = '';
			$scope.typeNameFilter = '';
		}

		$scope.goToPetDetail = (id) => {
			window.location = '/pet/id=' + id;
		}

		$scope.goToPets = () => {
			window.location = '/pets';
		}

		/**
         * Pagination
         */
        $scope.dirPaginateTemplate = parseView('directives.dir-pagination');
        $scope.currentPage = 1;
        $scope.pageSize = 12;
	}
})();