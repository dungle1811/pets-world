(function() {
    'use strict';

    angular
        .module('PetWorldModule', [
            'ui.router',
            'angularUtils.directives.dirPagination',
        ])
        .run(['$rootScope' ,'$state', ($rootScope, $state) => {

            $rootScope.current_state = 'index';

            $rootScope.$on('$stateChangeStart', (e, toState, toParams, fromtState, fromParams) => {

            });

            $rootScope.$on('$stateChangeSuccess', (e, toState, toParams, fromtState, fromParams) => {
                $rootScope.current_state = toState.name;
            });

            $rootScope.$on('$stateChangeFail', (e, toState, toParams, fromtState, fromParams) => {

            });
        }])
})();
