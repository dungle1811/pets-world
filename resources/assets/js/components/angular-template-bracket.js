(function() {
    'use strict';

    angular
        .module('PetWorldModule')
        .config(BracketConfig);

    BracketConfig.$inject = ['$interpolateProvider'];

    /* @ngInject */
    function BracketConfig($interpolateProvider) {
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
    }
})();
