<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'web'], function() {

    Route::get('/', [
        'as'     => 'view.home.index',
        'uses'   => 'HomepageController@getIndex'
    ]);

    Route::get('pets', [
        'as' => 'view.pets',
        'uses' => 'HomepageController@getPets'
    ]);    

    Route::get('pet/id={id}', [
        'as' => 'view.pet.detail',
        'uses' => 'HomepageController@getPetDetail'
    ]);

    Route::group(['prefix' => 'auth'], function() {

        Route::get('login', [
            'as' => 'view.auth.login',
            'uses' => 'Auth\LoginController@getLogin'
        ]);

        Route::post('login', [
            'as' => 'view.auth.login',
            'uses' => 'Auth\LoginController@postLogin'
        ]);

        Route::get('logout', [
            'as' => 'view.auth.logout',
            'uses' => 'Auth\LoginController@logout'
        ]);

        Route::get('register', [
            'as' => 'view.auth.register',
            'uses' => 'Auth\RegisterController@getRegister'
        ]);

        Route::post('register', [
            'as' => 'view.auth.register',
            'uses' => 'Auth\RegisterController@postRegister'
        ]);
        Route::get('/users/confirmation_token/{confirmation_token}', [
            'as'=>'view.emails.confirmation',
            'uses'=>'Auth\RegisterController@confirm',
        ]);
    });
});

Route::group(['middleware' => 'auth'], function() {

    Route::get('admin', [
        'as'    => 'view.admin',
        'uses'  => function(){
            return view('admin.index');
        }
    ]);
});

Route::get('/views/load', [
    'as' => 'views.load',
    'uses' => function (Illuminate\Http\Request $request) {
        $path = $request->get('path');
        return view($path)->render();
    }
]);
