<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get("pets/all",[
  'uses' => "Pet\PetController@all",
  'as'   => "api.pet.all",
]);

Route::get("pet-breeds/all",[
  'uses' => "Pet\PetBreedController@all",
  'as'   => "api.pet_breed.all",
]);

Route::get("pet-status/all",[
  'uses' => "Pet\PetStatusController@all",
  'as'   => "api.pet_status.all",
]);

Route::get("pet-types/all",[
  'uses' => "Pet\PetTypeController@all",
  'as'   => "api.pet_type.all",
]);

Route::get("shops/all",[
  'uses' => "Shop\ShopController@all",
  'as'   => "api.shop.all",
]);

Route::get("shop-status/all",[
  'uses' => "Shop\ShopStatusController@all",
  'as'   => "api.shop_status.all",
]);

Route::get("shop-services/all",[
  'uses' => "Shop\ShopServiceController@all",
  'as'   => "api.shop_service.all",
]);

Route::get("shop-accessory-categories/all",[
  'uses' => "Shop\ShopAccessoryCategoryController@all",
  'as'   => "api.shop_accessory_category.all",
]);

Route::get("shop-accessories/all",[
  'uses' => "Shop\ShopAccessoryController@all",
  'as'   => "api.shop_accessory.all",
]);

Route::get("blogs/all",[
  'uses' => "Blog\BlogController@all",
  'as'   => "api.blog.all",
]);

Route::get("blog-status/all",[
  'uses' => "Blog\BlogStatusController@all",
  'as'   => "api.blog_status.all",
]);

Route::get("blog-tags/all",[
  'uses' => "Blog\BlogTagsController@all",
  'as'   => "api.blog_tag.all",
]);

Route::get("pet-details/all",[
  'uses' => "Pet\PetDetailController@all",
  'as'   => "api.pet_detail.all",
]);

Route::get("pet-specifications/all",[
  'uses' => "Pet\PetSpecificationController@all",
  'as'   => "api.pet_specification.all",
]);

Route::get("pet-units/all",[
  'uses' => "Pet\PetUnitController@all",
  'as'   => "api.pet_unit.all",
]);

Route::get("pet-unit-types/all",[
  'uses' => "Pet\PetUnitTypeController@all",
  'as'   => "api.pet_unit_type.all",
]);

Route::group(['middleware' => ['auth:api']], function() {

  Route::group(['prefix' => 'auth'], function() {

    Route::get("get",[
      'uses' => "User\UserController@get",
      'as'   => "api.auth.get"
    ]);

    Route::post("update",[
      'uses' => "User\UserController@update",
      'as'   => "api.auth.update"
    ]);
  });

  Route::group(['prefix' => 'pets'], function() {

    Route::post("create",[
      'uses' => "Pet\PetController@create",
      'as'   => "api.pet.create",
    ]);

    Route::post("update",[
      'uses' => "Pet\PetController@update",
      'as'   => "api.pet.update",
    ]);

    Route::post("update/photo/landscape",[
      'uses' => "Pet\PetController@updateLandscapePhoto",
      'as'   => "api.pet.update.photo.landscape",
    ]);

    Route::post("update/photo/square",[
      'uses' => "Pet\PetController@updateSquarePhoto",
      'as'   => "api.pet.update.photo.square",
    ]);

    Route::post("delete", [
      'uses' => "Pet\PetController@delete",
      'as'   => "api.pet.delete",
    ]);
  });

  Route::group(['prefix' => 'pet-details'], function() {

    Route::post("create",[
      'uses' => "Pet\PetDetailController@create",
      'as'   => "api.pet_detail.create",
    ]);

    Route::post("update",[
      'uses' => "Pet\PetDetailController@update",
      'as'   => "api.pet_detail.update",
    ]);

    Route::post("delete", [
      'uses' => "Pet\PetDetailController@delete",
      'as'   => "api.pet_detail.delete",
    ]);
  });

  Route::group(['prefix' => 'pet-specifications'], function() {

    Route::post("save",[
      'uses' => "Pet\PetSpecificationController@save",
      'as'   => "api.pet_specification.save",
    ]);
  });

  Route::group(['prefix' => 'pet-breeds'], function() {

    Route::post("create",[
      'uses' => "Pet\PetBreedController@create",
      'as'   => "api.pet_breed.create",
    ]);

    Route::post("update",[
      'uses' => "Pet\PetBreedController@update",
      'as'   => "api.pet_breed.update",
    ]);

    Route::post("delete", [
      'uses' => "Pet\PetBreedController@delete",
      'as'   => "api.pet_breed.delete",
    ]);

    Route::post("delete/multiple", [
        'uses' => "Pet\PetBreedController@deleteMultiple",
        'as'   => "api.pet_breed_delete_multiple"
    ]);
  });

  Route::group(['prefix' => 'pet-status'], function() {

    Route::post("create",[
      'uses' => "Pet\PetStatusController@create",
      'as'   => "api.pet_status.create",
    ]);

    Route::post("update",[
      'uses' => "Pet\PetStatusController@update",
      'as'   => "api.pet_status.update",
    ]);

    Route::post("delete", [
      'uses' => "Pet\PetStatusController@delete",
      'as'   => "api.pet_status.delete",
    ]);

    Route::post("delete/multiple", [
        'uses' => "Pet\PetStatusController@deleteMultiple",
        'as'   => "api.pet_status_delete_multiple"
    ]);
  });

  Route::group(['prefix' => 'pet-types'], function() {

    Route::post("create",[
      'uses' => "Pet\PetTypeController@create",
      'as'   => "api.pet_type.create",
    ]);

    Route::post("update",[
      'uses' => "Pet\PetTypeController@update",
      'as'   => "api.pet_type.update",
    ]);

    Route::post("delete", [
      'uses' => "Pet\PetTypeController@delete",
      'as'   => "api.pet_type.delete",
    ]);

    Route::post("delete/multiple", [
        'uses' => "Pet\PetTypeController@deleteMultiple",
        'as'   => "api.pet_type_delete_multiple"
    ]);
  });

  Route::group(['prefix' => 'shops'], function() {

    Route::post("create",[
      'uses' => "Shop\ShopController@create",
      'as'   => "api.shop.create",
    ]);

    Route::post("update",[
      'uses' => "Shop\ShopController@update",
      'as'   => "api.shop.update",
    ]);

    Route::post("delete", [
      'uses' => "Shop\ShopController@delete",
      'as'   => "api.shop.delete",
    ]);
  });

  Route::group(['prefix' => 'shop-status'], function() {

    Route::post("create",[
      'uses' => "Shop\ShopStatusController@create",
      'as'   => "api.shop_status.create",
    ]);

    Route::post("update",[
      'uses' => "Shop\ShopStatusController@update",
      'as'   => "api.shop_status.update",
    ]);

    Route::post("delete", [
      'uses' => "Shop\ShopStatusController@delete",
      'as'   => "api.shop_status.delete",
    ]);
  });

  Route::group(['prefix' => 'shop-services'], function() {

    Route::post("create",[
      'uses' => "Shop\ShopServiceController@create",
      'as'   => "api.shop_service.create",
    ]);

    Route::post("update",[
      'uses' => "Shop\ShopServiceController@update",
      'as'   => "api.shop_service.update",
    ]);

    Route::post("delete", [
      'uses' => "Shop\ShopServiceController@delete",
      'as'   => "api.shop_service.delete",
    ]);
  });

  Route::group(['prefix' => 'shop-accessory-categories'], function() {

    Route::post("create",[
      'uses' => "Shop\ShopAccessoryCategoryController@create",
      'as'   => "api.shop_accessory_category.create",
    ]);

    Route::post("update",[
      'uses' => "Shop\ShopAccessoryCategoryController@update",
      'as'   => "api.shop_accessory_category.update",
    ]);

    Route::post("delete", [
      'uses' => "Shop\ShopAccessoryCategoryController@delete",
      'as'   => "api.shop_accessory_category.delete",
    ]);
  });

  Route::group(['prefix' => 'shop-accessories'], function() {

    Route::post("create",[
      'uses' => "Shop\ShopAccessoryController@create",
      'as'   => "api.shop_accessory.create",
    ]);

    Route::post("update",[
      'uses' => "Shop\ShopAccessoryController@update",
      'as'   => "api.shop_accessory.update",
    ]);

    Route::post("delete", [
      'uses' => "Shop\ShopAccessoryController@delete",
      'as'   => "api.shop_accessory.delete",
    ]);
  });

  Route::group(['prefix' => 'blogs'], function() {

    Route::post("create",[
      'uses' => "Blog\BlogController@create",
      'as'   => "api.blog.create",
    ]);

    Route::post("update",[
      'uses' => "Blog\BlogController@update",
      'as'   => "api.blog.update",
    ]);

    Route::post("delete", [
      'uses' => "Blog\BlogController@delete",
      'as'   => "api.blog.delete",
    ]);
  });

  Route::group(['prefix' => 'blog-status'], function() {

    Route::post("create",[
      'uses' => "Blog\BlogStatusController@create",
      'as'   => "api.blog_status.create",
    ]);

    Route::post("update",[
      'uses' => "Blog\BlogStatusController@update",
      'as'   => "api.blog_status.update",
    ]);

    Route::post("delete", [
      'uses' => "Blog\BlogStatusController@delete",
      'as'   => "api.blog_status.delete",
    ]);
  });

  Route::group(['prefix' => 'blog-tags'], function() {

    Route::post("create",[
      'uses' => "Blog\BlogTagsController@create",
      'as'   => "api.blog_tag.create",
    ]);

    Route::post("update",[
      'uses' => "Blog\BlogTagsController@update",
      'as'   => "api.blog_tag.update",
    ]);

    Route::post("delete", [
      'uses' => "Blog\BlogTagsController@delete",
      'as'   => "api.blog_tag.delete",
    ]);
  });
});
