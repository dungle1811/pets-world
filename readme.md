## Pet's world

This project is built based on Laravel and AngularJS.

## About Laravel

We are using Laravel 5.4 as the backend of the project. 

Readmore: [Laravel 5.4](https://github.com/laravel/laravel).

## About AnguarJS

We are using AngularJS 1.6.1 as the front end of the project. 

Readmore: [AngularJS 1.6.1](https://github.com/angular/code.angularjs.org/tree/master/1.6.1).

## More about the project

We are also using some Node.js module to help us in development phrase, such as: NPM, Gulp,... and many things more. If you want to know more about our project, please contact to our leader.

## About us

We are a team from DTU.

Members:

- Tri, Pham Van (Leader, Full-stack).
- Dung, Le Thi Thuy (Full-stack).
- Dung, Duong Cong (Back-end).
- Nguyen, Tran Hoang Phuoc (Front-end).
- Manh, Le Quoc (Front-end).

## Do you want to contribute?
We are looking for people who are interested in Pet's world project. If you want to give us a hand, we are welcome.